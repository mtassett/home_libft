#include "libft.h"
#include <ctype.h>

int main()
{
	int64_t	i = -128;

	while (i <= 1024)
	{
		if (!!ft_isalnum(i) != !!isalnum(i))
			printf("ft_isalnum(%ld) failed, obtained |%d| expected |%d|\n", (int)i, ft_isalnum(i), isalnum(i));
		i++;
	}
	return (0);
}
