/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utoh.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/17 16:57:29 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 22:33:59 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_utoh(uint64_t n)
{
	static char const	base16[] = "0123456789abcdef";
	char				*new;
	size_t				siz;

	siz = getsiz_hex(n);
	new = (char*)ft_xmalloc(siz + 1);
	new += siz;
	*(new--) = '\0';
	while (siz--)
	{
		*(new--) = base16[n % 16];
		n /= 16;
	}
	return (new);
}
