/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_realloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/17 21:18:04 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 22:38:14 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	vec_realloc(t_vec **vec)
{
	t_vec	*old;
	t_vec	*new;

	old = *vec;
	new = ft_xmalloc(sizeof(t_vec));
	new->v_lim = old->v_lim * 2;
	new->v_siz = old->v_siz;
	new->v_data = ft_xmalloc(new->v_lim * sizeof(void*));
	ft_memcpy(new->v_data, old->v_data, new->v_siz * sizeof(void*));
	free(old->v_data);
	free(old);
	*vec = new;
}
