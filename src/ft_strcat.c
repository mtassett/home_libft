/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/02 16:58:55 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 17:13:52 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The strcat() function appends the src string to the dest string, overwriting
** the terminating null byte ('\0') at the end of dest, and then adds a
** terminating null byte. The strings may not overlap, and the dest string must
** have enough space for the result. If dest is not large enough, program
** behavior is unpredictable; buffer overruns are a favorite avenue for
** attacking secure programs.
*/

char	*ft_strcat(char *dest, const char *src)
{
	char *tmp;

	tmp = dest;
	while (*tmp)
		tmp++;
	while ((*tmp = *src))
	{
		tmp++;
		src++;
	}
	*tmp = '\0';
	return (dest);
}
