/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putunbr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/29 22:21:32 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 21:58:28 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Print an unsigned char, based on the utoa fast alg.
*/

inline void	ft_putunbr(size_t un)
{
	char	tmp[32];
	size_t	siz;

	siz = ft_getulsiz(un);
	write(1, ft_utoa(tmp, un, siz), siz);
}
