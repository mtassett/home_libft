/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utoo.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/17 21:09:14 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 22:34:30 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_utoo(uint64_t n)
{
	static char	const	base8[] = "01234567";
	char				*new;
	size_t				siz;

	siz = getsiz_oct(n);
	new = ft_xmalloc(siz + 1);
	new += siz;
	*(new--) = '\0';
	while (siz--)
	{
		*(new--) = base8[n % 8];
		n /= 8;
	}
	return (new);
}
