/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_buf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/18 18:09:10 by mtassett          #+#    #+#             */
/*   Updated: 2017/02/10 21:56:12 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Work as itoa, but store the result in buf if non-NULL, else return
** a malloced str, and there isnt any error check
*/

char	*ft_itoa_buf(int n, char *buf)
{
	char const	dbase[] = "0123456789";
	size_t		siz;

	if (!buf)
		return (ft_itoa(n));
	if (n == 0)
		return (ft_memcpy(buf, "0", 2));
	if (n == INT_MIN)
		return (ft_memcpy(buf, "-2147483648", 12));
	siz = ft_getnsiz(n);
	buf[siz] = '\0';
	if (n < 0)
	{
		buf[0] = '-';
		n = -n;
	}
	while (n)
	{
		buf[siz-- - 1] = dbase[n % 10];
		n /= 10;
	}
	return (buf);
}
