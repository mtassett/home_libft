/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isblank.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 14:45:33 by mtassett          #+#    #+#             */
/*   Updated: 2022/05/02 13:36:37 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Check if c is a space or a tab
** Return true or false
*/

inline int	ft_isblank(int c)
{
	if ((unsigned char)c == ' ' || (unsigned char)c == '\t')
		return (1);
	else
		return (0);
}
