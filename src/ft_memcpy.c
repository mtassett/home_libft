/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/28 13:17:38 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 20:12:33 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The memcpy() function copies n bytes from memory area src to memory
** area dest. The memory areas must not overlap. Use memmove() if
** the memory areas do overlap.
*/

inline void	*ft_memcpy(void *dst, void const *src, size_t n)
{
	void *out;

	out = dst;
	while (n > sizeof(uint64_t))
	{
		*(uint64_t*)dst = *(uint64_t*)src;
		dst += sizeof(uint64_t);
		src += sizeof(uint64_t);
		n -= sizeof(uint64_t);
	}
	while (n--)
		*(uint8_t*)dst++ = *(uint8_t*)src++;
	return (out);
}
