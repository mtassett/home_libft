/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_getindex.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/17 22:06:59 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/17 22:56:48 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*vec_getindex(t_vec *vec, size_t index)
{
	if (index > vec->v_lim)
		return (NULL);
	return (*(vec->v_data + index));
}
