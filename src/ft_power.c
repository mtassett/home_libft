/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_power.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/15 20:21:56 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 17:05:25 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Naive implementation of a power function, take care of the overflow!
*/

size_t	ft_power(size_t base, size_t power)
{
	size_t result;

	result = 1;
	while (power)
	{
		if (power & 1)
			result *= base;
		power >>= 1;
		base *= base;
	}
	return (result);
}
