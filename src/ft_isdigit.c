/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isdigit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 17:59:59 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 19:11:13 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** checks for a digit (0 through 9), return true or false.
*/

inline int	ft_isdigit(int c)
{
	return (c <= '9' && c >= '0');
}
