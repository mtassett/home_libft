/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/14 23:11:21 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 20:13:41 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Free the memory adress pointed and set it to NULL, causing segfault if used
** after free.
*/

inline void		ft_memdel(void **ap)
{
	free(*ap);
	*ap = NULL;
}
