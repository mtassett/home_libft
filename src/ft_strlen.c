/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/02 16:43:42 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 17:23:21 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The strlen() function calculates the length of the string s, excluding the
** terminating null byte ('\0').
*/

size_t		ft_strlen(const char *s)
{
	register const char	*tmp;

	tmp = s;
	while (*tmp)
		tmp++;
	return (tmp - s);
}
