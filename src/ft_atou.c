/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atou.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 17:52:48 by mtassett          #+#    #+#             */
/*   Updated: 2016/08/12 19:06:54 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The atou() function converts the initial portion of the string pointed to
** by str to u32.
*/

uint32_t	ft_atou(const char *str)
{
	uint_fast32_t	result;
	uint_fast8_t	i;

	i = 0;
	result = 0;
	while (ft_isspace(str[i]))
		i++;
	if (str[i] == '+')
		++i;
	while (str[i])
	{
		if (str[i] >= '0' && str[i] <= '9')
			result = result * 10 + (str[i] - '0');
		else
			break;
		++i;
	}
	return (result);
}
