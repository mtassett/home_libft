/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isspace.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/10 21:41:03 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 19:11:41 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Check if c is a tab, newline, vtab, form feed, CR or space,
** return true or false.
*/

inline int	ft_isspace(int c)
{
	return ((c <= '\r' && c >= '\t') || c == ' ');
}
