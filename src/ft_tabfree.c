/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabfree.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/04 19:23:46 by mtassett          #+#    #+#             */
/*   Updated: 2017/01/04 19:25:33 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_tabfree(char ***tab)
{
	char	**tmp;

	tmp = *tab;
	if (tmp)
	{
		while (*tmp)
			free(*tmp++);
	}
	free(*tab);
	*tab = NULL;
}
