/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlst_lstins.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/28 00:57:03 by mtassett          #+#    #+#             */
/*   Updated: 2016/03/29 19:18:30 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Insert the new dlst to the end of dl
*/

void	ft_dlst_lstins(t_dlst *dl, t_dlst *new)
{
	while (dl->next)
		dl = dl->next;
	dl->next = new;
	new->prev = dl;
}
