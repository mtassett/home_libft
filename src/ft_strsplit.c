/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/17 00:12:34 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 22:29:30 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	get_sub_siz(const char *str, char c)
{
	const char	*tmp;

	tmp = str;
	while (*tmp != c && *tmp)
		tmp++;
	return (tmp - str);
}

static size_t	get_tab_row(const char *s, char c)
{
	uint16_t	n_sep;
	bool		word_before;

	word_before = false;
	n_sep = 0;
	while (*s)
	{
		if (*s == c)
		{
			while (*s == c)
				s++;
			if (word_before && *s)
				n_sep++;
			word_before = false;
		}
		else
		{
			word_before = true;
			s++;
		}
	}
	return (n_sep + 1);
}


char	**ft_strsplit(const char *s, char c)
{
	char	**out;
	size_t	n;
	size_t	row;
	size_t	siz;

	if (!s)
		return (NULL);
	row = get_tab_row(s, c);
	out = ft_xmalloc(sizeof(char *) * (row + 1));
	n = 0;
	while (*s == c)
		s++;
	while (n < row)
	{
		siz = get_sub_siz(s, c);
		out[n++] = siz ? ft_strsub(s, 0, siz) : NULL;
		s += siz;
		while(*s == c)
			s++;
	}
	out[n] = NULL;
	return (out);
}

/*
int main()
{
	char **tt;

	tt = ft_strsplit("***salut***!**", '*');
	if (strcmp(tt[0], "salut") == 0)
		printf("OK\n");
	else
		printf("KO\n");
	if (strcmp(tt[1], "!") == 0)
		printf("OK\n");
	else
		printf("KO\n");
	if (tt[2] == NULL)
		printf("OK\n");
	else
		printf("KO\n");
	free(tt);
	tt = ft_strsplit("********", '*');
	if (tt[0] == NULL)
		printf("OK\n");
	else
		printf("KO\n");
	tt = ft_strsplit("coucou", '*');
	if (strcmp(tt[0], "coucou") == 0)
		printf("OK\n");
	else
		printf("KO\n");
	free(tt);
	tt = ft_strsplit("coucou*****", '*');
	if (strcmp(tt[0], "coucou") == 0)
		printf("OK\n");
	else
		printf("KO\n");
	free(tt);
	tt = ft_strsplit("*****coucou", '*');
	if (strcmp(tt[0], "coucou") == 0)
		printf("OK\n");
	else
		printf("KO\n");
	free(tt);
	tt = ft_strsplit("", '*');
	if (tt && tt[0] == NULL)
		printf("OK\n");
	else
		printf("KO\n");
	free(tt);
	return (0);
}
*/
