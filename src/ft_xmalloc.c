/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xmalloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/20 21:29:55 by mtassett          #+#    #+#             */
/*   Updated: 2022/04/20 21:30:31 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

inline void	*ft_xmalloc(size_t siz)
{
	void	*new;

	new = malloc(siz);
	if (!new)
	{
		write(2, "ft_xmalloc: malloc of size ", 27);
		ft_putunbr(siz);
		write(2, " failed\n", 7);
		exit(EXIT_FAILURE);
	}
	return (new);
}
