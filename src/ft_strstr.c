/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 17:29:21 by mtassett          #+#    #+#             */
/*   Updated: 2015/11/19 22:00:40 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The strstr() function finds the first occurrence of the substring needle in
** the string haystack. The terminating null bytes ('\0') are not compared.
*/

char	*ft_strstr(const char *haystack, const char *needle)
{
	size_t i;

	i = 0;
	if (!*needle)
		return ((char*)haystack);
	while (*haystack)
	{
		while (haystack[i] == needle[i])
		{
			if (needle[i + 1] == '\0')
				return ((char*)haystack);
			i++;
		}
		haystack++;
		i = 0;
	}
	return (0);
}
