/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/02 16:47:36 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 22:00:57 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The strdup() function returns a pointer to a new string which is a duplicate
** of the string s. Memory for the new string is obtained with malloc(3), and
** can be freed with free(3).
*/

char	*ft_strdup(const char *src)
{
	char	*dest;
	char	*tmp;
	size_t	siz;

	siz = 0;
	while (src[siz])
		siz++;
	dest = (char*)ft_xmalloc(siz + 1);
	tmp = dest;
	while (siz > 8)
	{
		*(uint64_t*)tmp++ = *(uint64_t*)src++;
		siz -= 8;
	}
	while ((*tmp++ = *src++))
		;
	return (dest);
}
