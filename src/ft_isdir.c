/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isdir.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/22 22:55:10 by mtassett          #+#    #+#             */
/*   Updated: 2017/01/22 22:59:02 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <sys/stat.h>

int ft_isdir(const char *path)
{
	struct stat	buff;

	if (stat(path, &buff))
		return (EXIT_FAILURE);
	return (S_ISDIR(buff.st_mode));
}
