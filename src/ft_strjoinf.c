/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoinf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/15 00:28:52 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 22:01:57 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Join two strings, and free the originals.
*/

static void	worker(size_t siz, char *dst, char *src)
{
	while (siz > sizeof(uint64_t))
	{
		*(uint64_t*)dst = *(uint64_t*)src;
		dst += sizeof(uint64_t);
		src += sizeof(uint64_t);
		siz -= 8;
	}
	while (siz--)
		*dst++ = *src++;
}

char		*ft_strjoinf(char *s1, char *s2)
{
	char	*new;
	size_t	ss1;
	size_t	ss2;

	ss1 = ft_strlen2(s1);
	ss2 = ft_strlen2(s2);
	new = (char*)ft_xmalloc(ss1 + ss2 + 1);
	worker(ss1, new, s1);
	worker(ss2, new + ss1, s2);
	free(s1);
	free(s2);
	*(new + ss1 + ss2) = '\0';
	return (new);
}
