/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/14 23:37:57 by mtassett          #+#    #+#             */
/*   Updated: 2016/12/10 23:04:56 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Apply a function on each char of a string, ROT13 has never been so easy!
*/

void	ft_striter(char *s, void (*f)(char *))
{
	while (*s)
		f(s++);
}
