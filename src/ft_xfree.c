/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xfree.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/14 19:06:15 by mtassett          #+#    #+#             */
/*   Updated: 2016/11/14 19:44:51 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_xfree(size_t n, ...)
{
	va_list		va;
	void		*tmp;

	va_start(va, n);
	while (n--)
	{
		if ((tmp = va_arg(va, void*)))
			free(tmp);
	}
	va_end(va);
}
