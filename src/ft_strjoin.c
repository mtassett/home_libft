/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/15 00:28:52 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 22:01:32 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Join two strings, /!\ Memory leaks if misused /!\
*/

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*new;
	char	*tmp;
	size_t	ss1;
	size_t	ss2;

	ss1 = ft_strlen(s1);
	ss2 = ft_strlen(s2);
	new = ft_xmalloc(ss1 + ss2);
	tmp = new;
	while (ss1 > 8)
	{
		*(uint64_t*)tmp++ = *(uint64_t*)s1++;
		ss1 -= 8;
	}
	while (*s1)
		*tmp++ = *s1++;
	while (ss2 > 8)
	{
		*(uint64_t*)tmp++ = *(uint64_t*)s2++;
		ss2 -= 8;
	}
	while (*s2)
		*tmp++ = *s2++;
	return (new);
}
