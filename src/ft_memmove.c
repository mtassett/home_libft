/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/29 23:52:26 by mtassett          #+#    #+#             */
/*   Updated: 2016/04/28 19:40:32 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The memmove() function copies n bytes from memory area src to memory area
** dest. The memory areas may overlap: copying takes place as though the bytes
** in src are first copied into a temporary array that does not overlap src or
** dest, and the bytes are then copied from the temporary array to dest.
*/

void	*ft_memmove(void *dst, void const *src, size_t n)
{
	size_t	i;

	i = 0;
	if (dst > src)
	{
		while (i < n)
		{
			((char*)dst)[n - i - 1] = ((char*)src)[n - i - 1];
			++i;
		}
	}
	else
	{
		while (i < n)
		{
			((char*)dst)[i] = ((char*)src)[i];
			++i;
		}
	}
	return (dst);
}
