/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 21:06:48 by mtassett          #+#    #+#             */
/*   Updated: 2022/05/02 13:38:20 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Allocate and return a new element. content initialized with addr, next set
** to NULL.
*/

t_list	*ft_lstnew(void *addr)
{
	t_list	*new;

	new = (t_list*)ft_xmalloc(sizeof(t_list));
	new->content = addr;
	new->next = NULL;
	return (new);
}
