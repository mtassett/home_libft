/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/21 19:32:20 by mtassett          #+#    #+#             */
/*   Updated: 2022/04/21 19:37:16 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Delete and free the given element and every successor of that element,
** using the function del and free. Finally, the pointer to the list is
** set to NULL.
*/

void	ft_lstclear(t_list **alst, void (*del)(void *))
{
	t_list	*cur;
	t_list	*nxt;

	cur = *alst;
	while (cur)
	{
		nxt = cur->next;
		del(cur->content);
		free(cur);
		cur = nxt;
	}
	*alst = NULL;
}
