/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/15 18:48:12 by mtassett          #+#    #+#             */
/*   Updated: 2022/05/02 11:36:25 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Convert an int to a malloced string
*/

char	*ft_itoa(int n)
{
	char				*new;
	size_t				size;

	if (n == 0)
		return (ft_strdup("0"));
	if (n == INT_MIN)
		return (ft_strdup("-2147483648"));
	size = ft_getnsiz(n);
	new = (char*)ft_xmalloc(size + 1);
	new[size] = '\0';
	if (n < 0)
	{
		new[0] = '-';
		n = -n;
	}
	while (n)
	{
		new[size-- - 1] = "0123456789"[n % 10];
		n /= 10;
	}
	return (new);
}
