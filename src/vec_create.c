/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vecnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/17 20:37:31 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 22:36:46 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_vec	*vec_create(size_t siz)
{
	t_vec	*new;

	new = ft_xmalloc(sizeof(t_vec));
	new->v_data = ft_xmalloc(siz * sizeof(void*));
	new->v_siz = 0;
	new->v_lim = siz;
	return (new);
}
