/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tolower.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 18:11:39 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 17:26:25 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** If c is a uppercase letter, tolower() returns its lowercase equivalent, if a
** lowercase representation exists in the current locale. Otherwise, it returns
** c.
*/

inline int __attribute__((always_inline))	ft_tolower(int c)
{
	return (ft_isupper(c) ? c + ('a' - 'A') : c);
}
