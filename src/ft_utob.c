/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utob.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/17 21:09:14 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 22:33:29 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Convert an u64 to a malloced string with the result in binary (base2).
*/

char	*ft_utob(uint64_t n)
{
	static char	const	base2[] = "01";
	char				*new;
	size_t				siz;

	siz = getsiz_bin(n);
	new = ft_xmalloc(siz + 1);
	new += siz;
	*(new--) = '\0';
	while (n)
	{
		*(new--) = base2[n % 2];
		n /= 2;
	}
	return (new);
}
