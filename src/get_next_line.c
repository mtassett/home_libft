/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 21:34:10 by mtassett          #+#    #+#             */
/*   Updated: 2022/05/02 13:40:49 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		create_node(int fd, t_list **tmp, t_list **alst)
{
	t_node	*node;

	node = (t_node*)ft_xmalloc(sizeof(t_node));
	node->n_fd = fd;
	node->n_buf = ft_strnew(BUFF_SIZE_GNL);
	*tmp = ft_lstnew(node);
	ft_lstadd_front(&*alst, *tmp);
	free(node);
	return (1);
}

static size_t	ft_join(char **dst, char *src, t_node *node, bool jump)
{
	char	*out;
	char	*tmp_dst;
	char	*tmp_out;
	char	*tmp_src;

//	if (jump && *src == '\n')
//		tmp_src = src + 1;
//	else
//		tmp_src = src;
	tmp_src = (jump && *src == '\n') ? src + 1 : src;
	tmp_dst = *dst;
	out = ft_strnew(ft_strlen(src) + ft_strlen(tmp_dst));
	tmp_out = out;
	while (*tmp_dst)
		*tmp_out++ = *tmp_dst++;
	while (*tmp_src && *tmp_src != '\n')
		*tmp_out++ = *tmp_src++;
	free(*dst);
	*dst = out;
	ft_strncpy(node->n_buf, tmp_src, BUFF_SIZE_GNL);
	if (!jump)
		ft_bzero(src, BUFF_SIZE_GNL);
	return (ft_strlen(node->n_buf));
}

static int		gnl_worker(t_node *n, char **line, char *buf)
{
	int		s;

	if (ft_strlen(n->n_buf) > 0)
	{
		ft_join(line, n->n_buf, n, 1);
		if (*n->n_buf == '\n')
			return (1);
	}
	if (ft_strlen(n->n_buf))
		return (1);
	while ((s = read(n->n_fd, buf, BUFF_SIZE_GNL)))
	{
		if (s == -1)
			return (-1);
		ft_join(line, buf, n, 0);
		if (*n->n_buf == '\n')
			return (1);
	}
	return (ft_strchr(n->n_buf, '\n') || s || ft_strlen(*line));
}

static void	clear_node(t_list **alst, int fd)
{
	t_list	*curr;
	t_list	*prev;
	t_node	*node;

	prev = NULL;
	curr = *alst;
	while (curr)
	{
		node = (t_node*)curr->content;
		if (node->n_fd == fd)
		{
			free(node->n_buf);
			free(node);
			if (!prev)
				*alst = curr->next;
			else
				prev->next = curr->next;
			free(curr);
			return ;
		}
		prev = curr;
		curr = curr->next;
	}
}

int		get_next_line(int fd, char **line)
{
	static t_list	*alst;
	t_list			*tmp;
	char			*buf;
	int				ret;

	tmp = alst;
	if (!line || BUFF_SIZE_GNL < 1)
		return (-1);
	while (tmp && ((t_node*)tmp->content)->n_fd != fd)
		tmp = tmp->next;
	if (!tmp)
		if (create_node(fd, &tmp, &alst) == 0)
			return (-1);
	buf = ft_strnew(BUFF_SIZE_GNL);
	*line = ft_strnew(0);
	ret = gnl_worker((t_node*)tmp->content, line, buf);
	if (!ret)
		clear_node(&alst, fd);
	free(buf);
	return (ret);
}
