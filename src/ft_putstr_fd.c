/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/15 20:43:16 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 20:38:27 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Same as ft_putstr but with a specified fd
*/

inline void	ft_putstr_fd(char const *s, int fd)
{
	write(fd, s, ft_strlen(s));
}
