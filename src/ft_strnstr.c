/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 17:33:04 by mtassett          #+#    #+#             */
/*   Updated: 2015/11/20 19:17:28 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The strnstr() function locates the first occurrence of the null-terminated
** string little in the string big, where not more than len characters are
** searched. Characters that appear after a ‘\0’ character are not searched.
*/

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t	i;
	size_t	j;

	if (!*little)
		return ((char*)big);
	if (!big)
		return (NULL);
	i = 0;
	j = 0;
	while (big[i] && len--)
	{
		if (big[i] == little[j])
			while (big[i + j] == little[j] || !little[j])
			{
				if (little[j] == '\0')
					return ((char*)&(big[i]));
				if (j > len)
					return (NULL);
				j++;
			}
		i++;
		j = 0;
	}
	return (NULL);
}
