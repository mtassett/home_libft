/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_add.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/17 22:47:03 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/17 22:53:05 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	vec_add(t_vec *vec, void *addr)
{
	if (vec->v_siz == vec->v_lim)
		vec_realloc(&vec);
	*(vec->v_data + vec->v_siz++) = addr;
}
