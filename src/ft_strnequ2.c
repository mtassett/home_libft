/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/15 00:17:38 by mtassett          #+#    #+#             */
/*   Updated: 2016/05/17 20:26:01 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Check if two string are equal, check for NULL case too
*/

bool	ft_strnequ2(char const *s1, char const *s2, size_t n)
{
	if (!n || (!s1 && !s2))
		return (true);
	if ((!s1 && s2) || (s1 && !s2))
		return (false);
	while (*s1 == *s2 && *s1 && --n)
	{
		s1++;
		s2++;
	}
	return (*s1 != *s2 ? false : true);
}
