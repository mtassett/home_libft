/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/28 13:14:18 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 20:01:35 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The bzero() function sets the first n bytes of the area starting at s to
** zero (bytes containing '\0').
*/

inline void	ft_bzero(void *s, size_t n)
{
	uint64_t	*bp;
	uint8_t		*sp;

	bp = s;
	while (n > 8)
	{
		*bp++ = 0x0;
		n -= 8;
	}
	sp = (uint8_t*)bp;
	while (n--)
		*sp++ = 0x0;
}
