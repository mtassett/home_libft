/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strskip.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/25 21:18:23 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 21:24:57 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** ft_strskip return the index after skipping all char where the f function
** return true. The ft_is* functions can be used for example.
*/

size_t		ft_strskip(char *str, int (*f)(char))
{
	size_t	i;

	i = 0;
	while (*str && f(*str))
	{
		++i;
		++str;
	}
	return (i);
}
