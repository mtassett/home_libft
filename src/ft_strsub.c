/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/15 00:19:59 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 22:30:03 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Create a substring starting at the start index, with a given length
*/

char	*ft_strsub(char const *s, uint32_t start, size_t len)
{
	char	*new;

	new = ft_xmalloc(len + 1);
	*(new + len) = '\0';
	if (!len)
		return (new);
	while (len > 8)
	{
		len -= 8;
		*(uint64_t*)(new + len) = *(uint64_t*)(s + start + len);
	}
	while (len--)
		*(uint8_t*)(new + len) = *(uint8_t*)(s + start + len);
	return (new);
}

/*
int main()
{
	char txt[] = "Vive Les Canards";
	char *tmp;

	tmp = ft_strsub(txt, 5, 12);
	printf("12:%s\n", tmp);
	free(tmp);
	tmp = ft_strsub(txt, 5, 6);
	printf("6:%s\n", tmp);
	free(tmp);
	tmp = ft_strsub(txt, 0, 8);
	printf("8:%s\n", tmp);
	free(tmp);
	return (0);
}
*/
