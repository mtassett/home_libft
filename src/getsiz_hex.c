/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getsiz_hex.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/05 10:27:41 by mtassett          #+#    #+#             */
/*   Updated: 2016/06/24 20:09:29 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

inline static size_t	getsiz_hex2(uint64_t n, size_t r)
{
	if (n >= 0x100)
	{
		r += 2;
		n /= 0x100;
	}
	if (n >= 0x10)
		++r;
	return (r);
}

size_t					getsiz_hex(uint64_t n)
{
	size_t	r;

	r = 1;
	if (n >= 0x100000000)
	{
		r += 8;
		n /= 0x100000000;
	}
	if (n >= 0x10000)
	{
		r += 4;
		n /= 0x10000;
	}
	return (getsiz_hex2(n, r));
}
