/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/15 00:33:05 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 22:30:54 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Sanitize a string by removing space, <CR> and \t at the beginning and in the
** end. Return a new malloc'd string
*/

char		*ft_strtrim(char const *s)
{
	char	*new;
	size_t	i;
	size_t	j;
	size_t	k;

	i = 0;
	while ((s[i] == ' ' || s[i] == '\n' || s[i] == '\t') && s[i])
		i++;
	if (i == ft_strlen2(s))
		return (ft_strdup(""));
	j = ft_strlen2(s) - 1;
	while (s[j] == ' ' || s[j] == '\n' || s[j] == '\t')
		j--;
	if (!(new = ft_strnew(j - i + 1)))
		return (NULL);
	k = 0;
	while (i + k <= j)
	{
		new[k] = s[i + k];
		k++;
	}
	return (new);
}
