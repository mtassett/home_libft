/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getulsiz.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/21 18:58:11 by mtassett          #+#    #+#             */
/*   Updated: 2016/05/26 21:43:13 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Get the length of a number, up to uint64_t.
*/

inline static size_t	ft_getulsiz2(uint64_t nbr, size_t r)
{
	if (nbr >= 100)
	{
		r += 2;
		nbr /= 100;
	}
	if (nbr >= 10)
		++r;
	return (r);
}

size_t					ft_getulsiz(uint64_t nbr)
{
	size_t	r;

	r = 1;
	if (nbr >= 10000000000000000)
	{
		r += 16;
		nbr /= 10000000000000000;
	}
	if (nbr >= 100000000)
	{
		r += 8;
		nbr /= 100000000;
	}
	if (nbr >= 10000)
	{
		r += 4;
		nbr /= 10000;
	}
	return (ft_getulsiz2(nbr, r));
}
