/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/14 23:47:37 by mtassett          #+#    #+#             */
/*   Updated: 2016/12/10 23:26:11 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Apply a function on a string and store the result in another one of the
** same size
*/

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*new;
	char	*tmp;

	if (!(new = ft_strnew(ft_strlen(s))))
		return (NULL);
	tmp = new;
	while (*s)
		*tmp++ = f(*s++);
	return (new);
}
