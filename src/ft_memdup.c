/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/11 00:03:38 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 20:15:29 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memdup(const void *addr, size_t siz)
{
	void *new;

	new = ft_xmalloc(siz);
	ft_memcpy(new, addr, siz);
	return (new);
}
