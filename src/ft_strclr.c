/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strclr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/14 23:27:33 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 21:28:26 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Reset a string (it don't free it), it's faster to call ft_bzero
*/

inline void	ft_strclr(char *s)
{
	ft_bzero(s, ft_strlen(s));
}
