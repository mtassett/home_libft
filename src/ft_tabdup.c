/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/18 19:21:06 by mtassett          #+#    #+#             */
/*   Updated: 2016/12/18 21:20:40 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_tabdup(char **src, size_t siz)
{
	char	**new;
	size_t	i;

	if (!siz)
		siz = ft_tablen(src);
	new = ft_xmalloc(sizeof(char*) * (siz + 1));
	new[siz] = NULL;
	i = 0;
	while (i < siz)
	{
		new[i] = ft_strdup(src[i]);
		++i;
	}
	return (new);
}
