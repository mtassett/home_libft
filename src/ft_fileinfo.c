/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fileinfo.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/25 19:19:46 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 19:39:27 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <sys/stat.h>

/*
** Test if a file is of the given filetype, where filetype is the
** corresponding mask (S_IFDIR, S_IFREG, etc)
*/

int		ft_fileinfo(char *path, unsigned filetype)
{
	struct stat		st;

	if (stat(path, &st) == -1)
		return (-1);
	return (__S_ISTYPE(st.st_mode, filetype));
}
