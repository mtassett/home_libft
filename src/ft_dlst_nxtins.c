/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlst_nxtins.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/28 00:35:30 by mtassett          #+#    #+#             */
/*   Updated: 2016/03/29 19:21:29 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Insert new just after dl
*/

void	ft_dlst_nxtins(t_dlst *dl, t_dlst *new)
{
	new->next = dl->next;
	if (dl->next)
		dl->next->prev = new;
	new->prev = dl;
	dl->next = new;
}
