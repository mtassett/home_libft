/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/15 20:33:51 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 20:38:17 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Print a string, should be the more efficient way without using a buffer like
** printf
*/

inline void	ft_putstr(char const *s)
{
	write(1, s, ft_strlen(s));
}
