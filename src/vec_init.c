/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_init.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/17 22:41:22 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 22:37:13 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	vec_init(t_vec *vec, size_t siz)
{
	vec = ft_xmalloc(sizeof(t_vec));
	vec->v_data = ft_xmalloc(sizeof(void*) * siz);
}
