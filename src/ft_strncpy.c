/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/02 16:56:59 by mtassett          #+#    #+#             */
/*   Updated: 2016/06/06 17:56:24 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The strncpy() function is similar, except that at most n bytes of src are
** copied. Warning: If there is no null byte among the first n bytes of src, the
** string placed in dest will not be null-terminated.
** If the length of src is less than n, strncpy() writes additional null bytes
** to dest to ensure that a total of n bytes are written.
*/

char	*ft_strncpy(char *dest, const char *src, size_t n)
{
	size_t	siz;

	if (!n)
		return (dest);
	siz = ft_strlen(src);
	if (n <= siz)
		ft_memcpy(dest, src, n);
	else
	{
		ft_memcpy(dest, src, siz);
		ft_bzero(dest + siz, n - siz);
	}
	return (dest);
}
