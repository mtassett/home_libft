/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/02 17:01:59 by mtassett          #+#    #+#             */
/*   Updated: 2016/04/30 18:10:28 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Like strcat, but:
** -- it will use at most n bytes from src
** -- src does not need to be null-terminated if it contains n or more bytes
*/

char	*ft_strncat(char *dest, const char *src, size_t nb)
{
	char	*tmp;
	size_t	siz;

	if (!nb)
		return (dest);
	tmp = dest;
	siz = ft_strlen(src);
	while (*dest)
		dest++;
	nb = (siz > nb) ? nb : siz;
	while (nb > 8)
	{
		*(uint64_t*)dest++ = *(uint64_t*)src++;
		nb -= 8;
	}
	while ((*dest = *src) && nb--)
	{
		dest++;
		src++;
	}
	*dest = '\0';
	return (tmp);
}
