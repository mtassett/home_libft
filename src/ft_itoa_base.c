/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoabase.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/16 11:31:28 by mtassett          #+#    #+#             */
/*   Updated: 2018/06/23 02:04:01 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Convert an int64 to a string with base conversion,
** return NULL if the base is invalid (<2 OR >36)
*/

static void		worker(uint64_t val, uint64_t base, char *out, int *rank)
{
	if (val >= base)
		worker(val / base, base, out, rank);
	out[(*rank)++] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"[val % base];
}

static size_t	gsiz(int64_t n, int64_t base)
{
	size_t		siz;
	uint64_t	u64;

	siz = 1;
	if (base == 10)
	{
		if (n == INT64_MIN)
			return (20);
		if (n < 0)
			siz++;
		u64 = (n < 0) ? (uint64_t)-n : (uint64_t)n;
		while (u64 /= 10)
			siz++;
	}
	else
	{
		u64 = (uint64_t)n;
		while (u64 /= base)
			siz++;
	}
	return (siz);
}

char			*ft_itoa_base(int64_t num, int base)
{
	int			rank;
	uint64_t	val;
	char		*out;

	if (base < 2 || base > 36)
		return (NULL);
	out = ft_xmalloc(gsiz(num, (int64_t)base) + 1);
	rank = 0;
	if (base == 10)
	{
		if (num == INT64_MIN)
		{
			ft_strcpy(out, "-9223372036854775808");
			return (out);
		}
		if (num < 0)
			out[rank++] = '-';
		val = (num < 0) ? -num : num;
	}
	else
		val = (uint64_t)num;
	worker(val, (uint64_t)base, out, &rank);
	out[rank] = '\0';
	return (out);
}
