/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/28 13:34:55 by mtassett          #+#    #+#             */
/*   Updated: 2022/05/17 18:27:47 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The memccpy() function copies no more than n bytes from memory area
** src to memory area dest, stopping when the character c is found.
** Return a pointer to the next char in dst after c was found or NULL.
** If the memory areas overlap, the results are undefined.
*/

void	*ft_memccpy(void *dst, void const *src, int c, size_t n)
{
	while (n--)
		if ((*(uint8_t*)dst++ = *(uint8_t*)src++) == (uint8_t)c)
			return (dst);
	return (NULL);
}
