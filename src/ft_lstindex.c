/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstindex.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/25 19:55:05 by mtassett          #+#    #+#             */
/*   Updated: 2022/05/10 16:30:40 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstindex(t_list *head, size_t index)
{
	size_t	i;

	i = 1;
	while (head && i < index)
	{
		head = head->next;
		i++;
	}
	return (head);
}
