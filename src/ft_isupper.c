/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isupper.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 18:14:37 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 19:11:53 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** checks for an uppercase letter, return true or false.
*/

inline int	ft_isupper(int c)
{
	return (c >= 'A' && c <= 'Z');
}
