/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 22:07:48 by mtassett          #+#    #+#             */
/*   Updated: 2022/04/22 15:17:57 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Iterate the list lst and applies the function f to the content of each
** elements. Create a new list resulting of the successive applications of
** the function f. The function del is here to delete the content of an element
** if needed.
*/

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list	*head;
	t_list	*cur;

	head = NULL;
	if (lst)
	{
		if (!(head = ft_lstnew(f(lst->content))))
			return (head);
		if (del)
			del(lst->content);
		cur = head;
		lst = lst->next;
	}
	while (lst)
	{
		if (!(cur->next = ft_lstnew(f(lst->content))))
			return (cur->next);
		if (del)
			del(lst->content);
		lst = lst->next;
		cur = cur->next;
	}
	return (head);
}
