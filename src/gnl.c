/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gnl.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 21:34:10 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 22:35:29 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <fcntl.h>

static size_t	locate_separator(const char *str)
{
	const char	*tmp;

	tmp = str;
	while (*tmp != '\0' && *tmp != '\n')
		tmp++;
	return (tmp - str);
}

static size_t	ft_join(char **line, char *buf, char *bak, bool jmp)
{
	char	*out;
	size_t	buf_siz;
	size_t	line_siz;

	if (jmp && *buf == '\n')
		buf++;
	line_siz = ft_strlen2(*line);
	buf_siz = locate_separator(buf);
	out = ft_xmalloc(line_siz + buf_siz + 1);
	ft_memcpy(out, *line, line_siz);
	ft_memcpy(out + line_siz, buf, buf_siz);
	*(out + line_siz + buf_siz) = '\0';
	line_siz = ft_strlen(buf + buf_siz);
	ft_memcpy(bak, buf + buf_siz, line_siz);
	*(bak + line_siz) = '\0';
	if (*line)
		free(*line);
	*line = out;
	return (line_siz);
}

int		gnl(int fd, char **line)
{
	static char	bak[BUFF_SIZE_GNL + 1] = { 0 };
	char		buf[BUFF_SIZE_GNL + 1];
	int			ret;

	ft_bzero(buf, BUFF_SIZE_GNL + 1);
	if (!line || BUFF_SIZE_GNL < 1 || fd < 0)
		return (-1);
	*line = NULL;
	if (bak[0] != '\0')
		if (ft_join(line, bak, bak, true))
			return (1);
	while ((ret = read(fd, buf, BUFF_SIZE_GNL)))
	{
		if (ret == -1)
			return (-1);
		else if (ft_join(line, buf, bak, false))
			return (1);
	}
	return (ret);
}
