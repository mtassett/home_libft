/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_toupper.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 18:08:15 by mtassett          #+#    #+#             */
/*   Updated: 2022/04/21 21:55:26 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** If c is a lowercase letter, toupper() returns its uppercase equivalent, if
** an uppercase representation exists in the current locale. Otherwise, it
** returns c.
*/

inline int __attribute__((always_inline))	ft_toupper(int c)
{
	return (ft_islower(c) ? c + ('A' - 'a') : c);
}
