/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtoul.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/14 18:12:39 by mtassett          #+#    #+#             */
/*   Updated: 2017/06/16 14:41:12 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

uint64_t	ft_strtoul(char *buf)
{
	uint64_t	r;

	r = 0;
	while (ft_isspace(*buf))
		buf++;
	if (*buf == '+' || *buf == '-')
		buf++;
	while (ft_isalpha(*buf))
		r = 10 * r + *buf++ - '0';
	return (r);
}
