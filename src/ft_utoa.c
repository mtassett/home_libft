/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/29 20:53:56 by mtassett          #+#    #+#             */
/*   Updated: 2016/08/12 19:13:34 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** utoa fast implementation. /!\ No error detection /!\
** Use some lookup table to speed it up.
** ft_16set give the same object code as inlining memset
*/

static inline void	ft_16set(uint16_t *dst, uint16_t val)
{
	*dst = val;
}

char	*ft_utoa(char *buf, uint64_t n, size_t siz)
{
	static uint16_t const	table[100] = { UTOA };
	char					*p;
	size_t					prv;

	p = &buf[siz];
	*p = '\0';
	while (n >= 100)
	{
		prv = n;
		p -= 2;
		n /= 100;
		ft_16set((uint16_t*)p, table[prv - (n * 100)]);
	}
	if (siz % 2)
		*(--p) = n + '0';
	else
	{
		p -= 2;
		ft_16set((uint16_t*)p, table[n]);
	}
	return (p);
}

/*
int main()
{
	char *test;
	size_t value = 0;
	size_t siz = (size_t)snprintf(NULL, 0, "%lu", value);

	test = malloc(siz + 1);
	strncpy(test, "abcdefghijklmnopqrstuvwxyz", siz);
	ft_utoa(test, value, siz);
	for (int i = 0; i < siz; i++)
		printf("%c\n", test[i]);
	free(test);
	return (0);
}
*/
