/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getnsiz.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 21:14:10 by mtassett          #+#    #+#             */
/*   Updated: 2016/06/20 16:59:54 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Return the size of an int, '-' included.
*/

static inline size_t	ft_getnsiz2(int32_t n, size_t r)
{
	if (n >= 100)
	{
		r += 2;
		n /= 100;
	}
	if (n >= 10)
		++r;
	return (r);
}

size_t					ft_getnsiz(int32_t n)
{
	size_t	r;

	r = 1;
	if (n < 0)
	{
		++r;
		n = (n == INT_MIN) ? INT_MAX : -n;
	}
	if (n >= 100000000)
	{
		r += 8;
		n /= 100000000;
	}
	if (n >= 10000)
	{
		r += 4;
		n /= 10000;
	}
	return (ft_getnsiz2(n, r));
}
