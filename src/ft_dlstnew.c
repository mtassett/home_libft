/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlstnew.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/28 00:23:15 by mtassett          #+#    #+#             */
/*   Updated: 2022/05/06 11:07:44 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Create a new dlst, like ft_lstnew() but for dlst.
*/

t_dlst	*ft_dlstnew(void *content)
{
	t_dlst	*new;

	new = (t_dlst*)ft_xmalloc(sizeof(t_dlst));
	new->content = (content) ? content : NULL;
	new->next = NULL;
	new->prev = NULL;
	return (new);
}
