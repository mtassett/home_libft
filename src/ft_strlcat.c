/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/02 17:03:37 by mtassett          #+#    #+#             */
/*   Updated: 2016/05/02 17:37:35 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The strlcat() function appends the NUL-terminated string src to the end of
** dst. It will append at most size - strlen(dst) - 1 bytes, NUL-terminating
** the result.
*/

size_t	ft_strlcat(char *dest, const char *src, size_t size)
{
	char	*tmp_d;
	char	*tmp_s;
	size_t	nb;
	size_t	dest_len;

	tmp_d = dest;
	tmp_s = (char*)src;
	nb = size;
	while (*tmp_d && nb--)
		tmp_d++;
	dest_len = tmp_d - dest;
	nb = size - dest_len;
	if (!nb)
		return (dest_len + ft_strlen(src));
	while (*tmp_s)
	{
		if (nb != 1)
		{
			*tmp_d++ = *tmp_s;
			nb--;
		}
		tmp_s++;
	}
	*tmp_d = '\0';
	return (dest_len + (tmp_s - src));
}
