/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalnum.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 18:02:03 by mtassett          #+#    #+#             */
/*   Updated: 2022/05/02 13:37:03 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** checks for an alphanumeric character, return true or false
*/

inline int	ft_isalnum(int c)
{
	return ((((uint16_t)c >= 'a' && (uint16_t)c <= 'z')
				|| ((uint16_t)c >= 'A' && (uint16_t)c <= 'Z')
				|| ((uint16_t)c >= '0' && (uint16_t)c <= '9')));
}
