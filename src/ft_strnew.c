/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/14 23:14:18 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 22:02:26 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Allocate size + 1 of memory and clean it before returning
*/

char	*ft_strnew(size_t size)
{
	char	*new;

	new = (char *)ft_xmalloc(size + 1);
	ft_bzero(new, size + 1);
	return (new);
}
