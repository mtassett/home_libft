/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/14 23:07:18 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 21:56:39 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Allocate the asked memory, and initialize it to 0
*/

inline void	*ft_memalloc(size_t size)
{
	void	*mem;

	mem = ft_xmalloc(size);
	ft_bzero(mem, size);
	return (mem);
}
