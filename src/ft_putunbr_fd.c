/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putunbr_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/29 22:21:32 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 21:59:02 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Print an unsigned number on the specified fd, use the utoa fast alg.
*/

inline void	ft_putunbr_fd(size_t un, int fd)
{
	char	tmp[32];
	size_t	siz;

	siz = ft_getulsiz(un);
	write(fd, ft_utoa(tmp, un, siz + 1), siz);
}
