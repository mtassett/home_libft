/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlst_prvins.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/28 00:41:30 by mtassett          #+#    #+#             */
/*   Updated: 2016/03/29 19:25:36 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Insert new just before dl
*/

void	ft_dlst_prvins(t_dlst *dl, t_dlst *new)
{
	new->prev = dl->prev;
	if (dl->prev)
		dl->prev->next = new;
	new->next = dl;
	dl->prev = new;
}
