/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/15 00:17:38 by mtassett          #+#    #+#             */
/*   Updated: 2016/04/29 22:10:47 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Check if two string are equal, no NULL test, look at ft_strnequ2 for that.
*/

bool	ft_strnequ(char const *s1, char const *s2, size_t n)
{
	if (!n)
		return (true);
	while (*s1 == *s2 && *s1 && --n)
	{
		s1++;
		s2++;
	}
	return (*s1 != *s2 ? false : true);
}
