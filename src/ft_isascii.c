/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isascii.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 18:03:32 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 19:10:54 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** checks whether c is a 7-bit unsigned char value that fits into the
** ASCII character set. Return true or false.
*/

inline int	ft_isascii(int c)
{
	return ((c >= 0 && c <= 127));
}
