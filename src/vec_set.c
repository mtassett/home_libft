/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_set.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/17 20:55:46 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/17 23:04:22 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Set the index element to addr. Do not modify the current size, be carreful
** when using it!
*/

int		vec_set(t_vec *vec, void *new, size_t index)
{
	if (index > vec->v_lim)
		return (EXIT_FAILURE);
	if (vec->v_siz == vec->v_lim)
		vec_realloc(&vec);
	*(vec->v_data + index) = new;
	return (EXIT_SUCCESS);
}
