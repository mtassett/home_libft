/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/02 16:24:42 by mtassett          #+#    #+#             */
/*   Updated: 2016/05/26 22:15:57 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The memchr() function scans the initial n bytes of the memory area pointed
** to by s for the first instance of c. Both c and the bytes of the memory area
** pointed to by s are interpreted as unsigned char.
*/

void	*ft_memchr(void const *s, int c, size_t n)
{
	while (n--)
	{
		if (*(uint8_t*)s == (uint8_t)c)
			return ((void*)s);
		++s;
	}
	return (NULL);
}
