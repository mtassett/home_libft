/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 18:58:11 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 17:14:39 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** strcmp implementation, handling NULL case
*/

int		ft_strcmp2(char const *s1, char const *s2)
{
	if (s1 && !s2)
		return ((unsigned char)*s1);
	if (!s1 && s2)
		return (-(unsigned char)*s2);
	while (*s1 == *s2 && *s1)
	{
		s1++;
		s2++;
	}
	return ((unsigned char)*s1 - (unsigned char)*s2);
}
