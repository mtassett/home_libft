/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/02 16:43:42 by mtassett          #+#    #+#             */
/*   Updated: 2016/11/17 18:40:35 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The strlen() function calculates the length of the string s, excluding the
** terminating null byte ('\0'). This function handle NULL case by returning 0
*/

size_t		ft_strlen2(const char *s)
{
	const char	*tmp;

	if (!s || *s == '\0')
		return (0);
	tmp = s;
	while (*tmp)
		tmp++;
	return (tmp - s);
}
