/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_islower.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 18:16:39 by mtassett          #+#    #+#             */
/*   Updated: 2022/05/02 13:37:25 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** checks for a lowercase character, true or false
*/

inline int	ft_islower(int c)
{
	if (c > -128 && c < 256)
		if (c <= 'z' && c >= 'a')
			return (1);
	return (0);
}
