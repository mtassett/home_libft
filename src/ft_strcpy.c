/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/02 16:53:43 by mtassett          #+#    #+#             */
/*   Updated: 2015/11/04 17:13:08 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The strcpy() function copies the string pointed to by src, including the
** terminating null byte ('\0'), to the buffer pointed to by dest. The strings
** may not overlap, and the destination string dest must be large enough to
** receive the copy. Beware of buffer overruns!
*/

char	*ft_strcpy(char *dest, const char *src)
{
	char *tmp;

	tmp = dest;
	while ((*dest++ = *src++))
		;
	return (tmp);
}
