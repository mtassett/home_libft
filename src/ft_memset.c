/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/28 13:31:10 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 20:03:49 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The memset() function fills the first n bytes of the memory area
** pointed to by s with the constant byte c. Fills 8 by 8 and finish 1 by 1.
*/

inline void	*ft_memset(void *p, int c, size_t n)
{
	uint64_t	u64;
	uint64_t	*bp;
	uint8_t		*sp;

	bp = p;
	c = (unsigned char)c;
	u64 = (c << 8) | c;
	u64 = (u64 << 16) | u64;
	u64 = (u64 << 32) | u64;
	while (n > 8)
	{
		*bp++ = u64;
		n -= 8;
	}
	sp = (uint8_t*)bp;
	while (n--)
		*sp++ = c;
	return (p);
}
