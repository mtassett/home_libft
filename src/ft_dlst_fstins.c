/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlst_fstins.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/28 00:54:42 by mtassett          #+#    #+#             */
/*   Updated: 2022/05/06 10:48:26 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Insert the new dlst at the start of the dl dlst. dl need to be alloced.
*/

void	ft_dlst_fstins(t_dlst *dl, t_dlst *new)
{
	while (dl->prev)
		dl = dl->prev;
	dl->prev = new;
	new->next = dl;
}
