/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strxjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/27 12:23:47 by mtassett          #+#    #+#             */
/*   Updated: 2016/10/06 20:51:17 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Magical strjoin able to join as much strings as specified
*/

char	*ft_strxjoin(size_t n, ...)
{
	va_list	va;
	size_t	size;
	size_t	i;
	char	*new;
	char	*tab[n];

	size = 0;
	i = n;
	va_start(va, n);
	while (i--)
	{
		tab[i] = va_arg(va, char *);
		size += ft_strlen(tab[i]);
	}
	va_end(va);
	new = ft_strnew(size + 1);
	while (n--)
		ft_strcat(new, tab[n]);
	return (new);
}
