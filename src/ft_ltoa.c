/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ltoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/15 18:48:12 by mtassett          #+#    #+#             */
/*   Updated: 2017/06/16 14:37:51 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Convert an i64 to a malloced string
*/

char	*ft_ltoa(int64_t n)
{
	char				*new;
	size_t				size;

	if (n == 0)
		return (ft_memdup("0", 2));
	if (n == INT64_MIN)
		return (ft_memdup("-9223372036854775808", 21));
	size = ft_getlsiz(n);
	new = (char*)ft_xmalloc(size + 1);
	if (n < 0)
	{
		new[0] = '-';
		n = -n;
	}
	while (n)
	{
		new[size-- - 1] = "0123456789"[n % 10];
		n /= 10;
	}
	return (new);
}
