/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/21 18:06:58 by mtassett          #+#    #+#             */
/*   Updated: 2022/04/21 18:22:16 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_calloc(size_t nmemb, size_t size)
{
	size_t	tsiz;
	void	*out;
	
	if (!size || nmemb > SIZE_MAX / size)
		return (NULL);
	tsiz = nmemb * size;
	if ((out = malloc(tsiz)))
		ft_bzero(out, tsiz);
	return (out);
}
