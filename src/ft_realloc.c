/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/17 22:21:22 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 20:45:34 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_realloc(void **addr, size_t oldsiz, size_t newsiz)
{
	void	*new;

	new = ft_xmalloc(newsiz);
	ft_memcpy(new, *addr, oldsiz);
	free(*addr);
	*addr = new;
	return (EXIT_SUCCESS);
}
