/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putlst.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/25 20:40:04 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 20:41:32 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_putlst(t_list *head)
{
	while (head)
	{
		write(1, (char*)head->content, ft_strlen(head->content));
		write(1, "\n", 1);
		head = head->next;
	}
}
