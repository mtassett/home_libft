/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/02 16:38:34 by mtassett          #+#    #+#             */
/*   Updated: 2016/06/06 17:56:57 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The memcmp() function compares the first n bytes (each interpreted as
** unsigned char) of the memory areas s1 and s2.
*/

int		ft_memcmp(void const *s1, void const *s2, size_t n)
{
	if (!n)
		return (0);
	while (n > 8)
	{
		if (*(uint64_t*)s1 != *(uint64_t*)s2)
			break ;
		s1 += 8;
		s2 += 8;
		n -= 8;
	}
	while (*(uint8_t*)s1 == *(uint8_t*)s2 && --n)
	{
		s1++;
		s2++;
	}
	return (*(uint8_t*)s1 - *(uint8_t*)s2);
}
