/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 16:35:16 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 21:27:35 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The strchr() function returns a pointer to the first occurrence of the
** character c in the string s. If c is specified as '\0', it return a pointer
** to the terminator.
*/

char	*ft_strchr(const char *s, char c)
{
	while (*s && *s != c)
		s++;
	return ((*s == c) ? (char *)s : NULL);
}
