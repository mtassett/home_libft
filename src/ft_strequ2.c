/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/15 00:11:16 by mtassett          #+#    #+#             */
/*   Updated: 2016/04/29 22:08:46 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Check if two string are equal or not. Return True or False. Handle NULL case
*/

bool	ft_strequ2(char const *s1, char const *s2)
{
	if (!s1 && !s2)
		return (true);
	else if ((s1 && !s2) || (!s1 && s2))
		return (false);
	while (*s1 == *s2 && *s1)
	{
		s1++;
		s2++;
	}
	return ((*s1 != *s2) ? false : true);
}
