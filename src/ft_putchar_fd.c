/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/15 20:41:55 by mtassett          #+#    #+#             */
/*   Updated: 2017/05/25 20:37:36 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Print a char to a designed fd.
** Pretty slow, don't use it for big writes
*/

inline void		ft_putchar_fd(char c, int fd)
{
	write(fd, &c, 1);
}
