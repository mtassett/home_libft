/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fileperm.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/25 19:39:32 by mtassett          #+#    #+#             */
/*   Updated: 2022/05/06 17:53:15 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <sys/stat.h>

/*
** test a file for permissions, where mode is using  S_IXUSR, S_IWGRP, S_IROTH
** like macro. You can test multiple at once using | (e.g. S_IXUSR | S_IWUSR)
*/

int		ft_fileperm(char *path, unsigned mode)
{
	struct stat		st;

	if (stat(path, &st) == -1)
		return (-1);
	return (st.st_mode & mode);
}
