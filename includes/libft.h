/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 18:07:31 by mtassett          #+#    #+#             */
/*   Updated: 2022/05/17 19:06:38 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# include <limits.h>
# include <stdarg.h>
# include <stdbool.h>
# include <stdlib.h>
# include <stdint.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <memwatch.h>
# define TAB1 0x3030, 0x3130, 0x3230, 0x3330, 0x3430, 0x3530, 0x3630, 0x3730,
# define TAB2 0x3830, 0x3930, 0x3031, 0x3131, 0x3231, 0x3331, 0x3431, 0x3531,
# define TAB3 0x3631, 0x3731, 0x3831, 0x3931, 0x3032, 0x3132, 0x3232, 0x3332,
# define TAB4 0x3432, 0x3532, 0x3632, 0x3732, 0x3832, 0x3932, 0x3033, 0x3133,
# define TAB5 0x3233, 0x3333, 0x3433, 0x3533, 0x3633, 0x3733, 0x3833, 0x3933,
# define TAB6 0x3034, 0x3134, 0x3234, 0x3334, 0x3434, 0x3534, 0x3634, 0x3734,
# define TAB7 0x3834, 0x3934, 0x3035, 0x3135, 0x3235, 0x3335, 0x3435, 0x3535,
# define TAB8 0x3635, 0x3735, 0x3835, 0x3935, 0x3036, 0x3136, 0x3236, 0x3336,
# define TAB9 0x3436, 0x3536, 0x3636, 0x3736, 0x3836, 0x3936, 0x3037, 0x3137,
# define TAB0 0x3237, 0x3337, 0x3437, 0x3537, 0x3637, 0x3737, 0x3837, 0x3937,
# define TABA 0x3038, 0x3138, 0x3238, 0x3338, 0x3438, 0x3538, 0x3638, 0x3738,
# define TABB 0x3838, 0x3938, 0x3039, 0x3139, 0x3239, 0x3339, 0x3439, 0x3539,
# define TABC 0x3639, 0x3739, 0x3839, 0x3939
# define UTOA TAB1 TAB2 TAB3 TAB4 TAB5 TAB6 TAB7 TAB8 TAB9 TAB0 TABA TABB TABC
# define CPU_RELAX(); asm volatile ("yield" :: "memory");
# define BUFF_SIZE_GNL	64

typedef __int128_t	t_int128;
typedef	__uint128_t	t_uint128;

/*
** Linked list with default void *ptr
*/

typedef struct		s_list
{
	void			*content;
	struct s_list	*next;
}					t_list;

/*
** Double linked list, works the same way as t_list
*/

typedef struct		s_dlst
{
	void			*content;
	struct s_dlst	*prev;
	struct s_dlst	*next;
}					t_dlst;

/*
** Binary Tree
*/

typedef struct		s_btree
{
	void			*content;
	size_t			content_size;
	struct s_btree	*left;
	struct s_btree	*right;
}					t_btree;

/*
** Custom linked list needed by the multi-fd GNL
*/

typedef struct		s_node
{
	int				n_fd;
	char			*n_buf;
}					t_node;

/*
** Contain a string and a defined size, used in ft_printf,
** allow a better and faster handling throught ft_mem fct.
*/

typedef struct		s_sstr
{
	char			*s_str;
	size_t			s_siz;
}					t_str;

/*
** Implementation of vectors
*/

typedef struct		s_vec
{
	void			**v_data;
	size_t			v_siz;
	size_t			v_lim;
}					t_vec;

/*
** Char
*/

int					ft_isalnum(int c);
int					ft_isalpha(int c);
int					ft_isascii(int c);
int					ft_isblank(int c);
int					ft_isdigit(int c);
int					ft_islower(int c);
int					ft_isprint(int c);
int					ft_isspace(int c);
int					ft_isupper(int c);
int					ft_isxdigit(int c);
int					ft_isblank(int c);
int					ft_iscntrl(int c);
int					ft_ispunct(int c);
int					ft_isgraph(int c);
int					ft_tolower(int c);
int					ft_toupper(int c);

/*
** Base conversion
*/

int					ft_atoi(const char *str);
char				*ft_itoa(int32_t n);
char				*ft_itoa_base(int64_t value, int base);
char				*ft_itoa_buf(int n, char *buf);
int64_t				ft_atol(const char *buf);
uint32_t			ft_atou(const char *str);
uint64_t			ft_atoul(const char *buf);
char				*ft_utoa(char *buf, uint64_t n, size_t siz);
char				*ft_ltoa(int64_t n);
char				*ft_utob(uint64_t u);
char				*ft_utoh(uint64_t u);
char				*ft_utoo(uint64_t u);

/*
** Memory
*/

void				ft_bzero(void *s, size_t n);
void				*ft_memalloc(size_t size);
void				*ft_memccpy(void *dest, const void *src, int c, size_t n);
void				*ft_memchr(const void *s, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
void				*ft_memdup(const void *data, size_t siz);
void				*ft_memcpy(void *dest, const void *src, size_t n);
void				ft_memdel(void **ap);
void				*ft_memmove(void *dest, const void *src, size_t n);
void				*ft_memset(void *s, int c, size_t n);
int					ft_realloc(void **addr, size_t oldsiz, size_t newsiz);
void				*ft_calloc(size_t nmemb, size_t size);
void				*ft_print_memory(void *addr, size_t n);

/*
** Math
*/

size_t				ft_power(size_t base, size_t exp);

/*
** Print
*/

void				ft_putchar(char c);
void				ft_putstr(char const *s);
void				ft_putendl(char const *s);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putendl_fd(char const *s, int fd);
void				ft_puttab(char **tab);
void				ft_putlst(t_list *head);
void				ft_putnbr(int n);
void				ft_putnbr_fd(int n, int fd);
void				ft_putunbr(size_t un);
void				ft_putunbr_fd(size_t un, int fd);

/*
** Strings
*/

char				*ft_strcat(char *dest, const char *src);
char				*ft_strchr(const char *s, char c);
void				ft_strclr(char *s);
int					ft_strcmp(const char *s1, const char *s2);
char				*ft_strcpy(char *dest, const char *src);
void				ft_strdel(char **as);
char				*ft_strdup(const char *s);
bool				ft_strequ(char const *s1, char const *s2);
void				ft_striter(char *s, void (*f)(char *));
void				ft_striteri(char *s, void (*f)(unsigned int, char *));
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_strjoinf(char *s1, char *s2);
char				*ft_strxjoin(size_t n, ...);
size_t				ft_strlcat(char *dst, const char *src, size_t size);
size_t				ft_strlen(const char *s);
char				*ft_strmap(char const *s, char (*f)(char));
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
char				*ft_strncat(char *dest, const char *src, size_t n);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
char				*ft_strncpy(char *dest, const char *src, size_t n);
bool				ft_strnequ(char const *s1, char const *s2, size_t n);
char				*ft_strnew(size_t size);
char				*ft_strnstr(const char *big, const char *lit, size_t len);
char				*ft_strrchr(const char *s, int c);
char				**ft_strsplit(char const *s, char c);
char				*ft_strstr(const char *haystack, const char *needle);
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_strtrim(char const *s);

/*
** tabs
*/

size_t				ft_tablen(char **tab);
char				**ft_tabdup(char **src, size_t siz);
void				ft_tabfree(char ***arr_addr);

/*
** wchar_t
*/

size_t				ft_wcslen(const wchar_t *str);

/*
** NULL case str
*/

int					ft_strcmp2(char const *s1, char const *s2);
bool				ft_strequ2(char const *s1, char const *s2);
size_t				ft_strlen2(char const *str);
bool				ft_strnequ2(char const *s1, char const *s2, size_t n);

/*
** Numbers
*/

size_t				ft_getnsiz(int32_t n);
size_t				ft_getlsiz(int64_t n);
size_t				ft_getunsiz(uint32_t u);
size_t				ft_getulsiz(uint64_t u);
size_t				getsiz_bin(uint64_t u);
size_t				getsiz_oct(uint64_t u);
size_t				getsiz_hex(uint64_t u);

/*
** List
*/

t_list				*ft_lstnew(void *content);
void				ft_lstdelone(t_list *alst, void (*del)(void*));
void				ft_lstclear(t_list **alst, void (*del)(void*));
void				ft_lstadd_front(t_list **alst, t_list *new);
void				ft_lstadd_back(t_list **alst, t_list *new);
size_t				ft_lstlen(t_list *lst);
t_list				*ft_lstlast(t_list *lst);
void				ft_lstiter(t_list *lst, void (*f)(void *));
t_list				*ft_lstmap(t_list *lst, void *(*f)(void *),
		void (*del)(void *));
t_list				*ft_lstindex(t_list *alst, size_t i);
void				ft_del(void *addr);

/*
** ft_printf
*/

int					ft_printf(const char *restrict str, ...);

/*
** get_next_line (multi-fd)
*/

int					get_next_line(int fd, char **line);

/*
** gnl.c (no multi-fd)
*/

int					gnl(int fd, char **line);

/*
** Double Linked List
*/

t_dlst				*ft_dlstnew(void *content);
void				ft_dlst_nxtins(t_dlst *dl, t_dlst *new);
void				ft_dlst_prvins(t_dlst *dl, t_dlst *new);
void				ft_dlst_fstins(t_dlst *dl, t_dlst *new);
void				ft_dlst_lstins(t_dlst *dl, t_dlst *new);

/*
** Vectors
*/

t_vec				*vec_create(size_t siz);
void				vec_init(t_vec *vec, size_t siz);
void				vec_realloc(t_vec **vec);
void				vec_add(t_vec *vec, void *addr);
void				vec_free(t_vec **vec);
void				*vec_getindex(t_vec *vec, size_t index);
int					vec_set(t_vec *vec, void *addr, size_t index);

/*
** Misc
*/

void				ft_xfree(size_t n, ...);
void				*ft_xmalloc(size_t siz);

/*
** Files
*/

int					ft_fileinfo(char *path, unsigned type);
int					ft_fileperm(char *path, unsigned perm);
int					ft_isdir(const char *path);
int					ft_isfile(const char *path);

#endif
