# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/05/09 21:33:56 by mtassett          #+#    #+#              #
#*   Updated: 2022/05/17 19:06:02 by mtassett         ###   ########.fr       *#
#                                                                              #
# **************************************************************************** #

NAME := libft.a
FTPRINTF :=	libftprintf.a

SRC_PATH :=	./src/
SRC_NAME := ft_atoi.c ft_atol.c ft_atou.c ft_atoul.c ft_itoa.c\
		   ft_itoa_base.c ft_itoa_buf.c ft_ltoa.c ft_bzero.c\
		   ft_getlsiz.c ft_getnsiz.c ft_getulsiz.c ft_getunsiz.c\
		   ft_isalnum.c ft_isalpha.c ft_isascii.c ft_isblank.c ft_isdigit.c\
		   ft_islower.c ft_isprint.c ft_isspace.c ft_isupper.c ft_isxdigit.c\
		   ft_ispunct.c ft_isgraph.c ft_iscntrl.c\
		   ft_dlstnew.c ft_dlst_lstins.c ft_dlst_fstins.c ft_dlst_nxtins.c ft_dlst_prvins.c\
		   ft_fileperm.c ft_fileinfo.c\
		   ft_lstclear.c ft_lstdelone.c ft_lstlen.c ft_lstlast.c ft_lstindex.c\
		   ft_lstiter.c ft_lstmap.c ft_lstnew.c ft_lstadd_front.c ft_lstadd_back.c\
		   ft_isdir.c ft_isfile.c\
		   ft_lstdelone.c ft_lstiter.c ft_lstmap.c ft_lstnew.c\
		   ft_memalloc.c ft_memccpy.c\
		   ft_memchr.c ft_memcmp.c ft_memcpy.c ft_memdel.c ft_memdup.c ft_memmove.c\
		   ft_memset.c ft_print_memory.c ft_putchar.c ft_putchar_fd.c\
		   ft_putendl.c ft_putendl_fd.c ft_putnbr.c\
		   ft_putnbr_fd.c ft_putstr.c ft_putstr_fd.c ft_putunbr.c ft_putunbr_fd.c\
		   ft_realloc.c\
		   ft_strcat.c ft_strccnt.c ft_strchr.c ft_strclr.c\
		   ft_strcmp2.c ft_strcmp.c ft_strcpy.c ft_strdel.c\
		   ft_strdup.c ft_strequ2.c ft_strequ.c ft_striter.c\
		   ft_striteri.c ft_strjoin.c ft_strjoinf.c ft_strlcat.c\
		   ft_strlen2.c ft_strlen.c ft_strmap.c ft_strmapi.c\
		   ft_strncat.c ft_strncmp.c ft_strncpy.c ft_strnequ2.c ft_strnequ.c\
		   ft_strnew.c ft_strnstr.c ft_strrchr.c ft_strsplit.c\
		   ft_strstr.c ft_strsub.c ft_strtoul.c\
		   ft_strtrim.c ft_strxjoin.c\
		   ft_tablen.c ft_tabdup.c ft_tabfree.c ft_tolower.c ft_toupper.c\
		   ft_utoa.c ft_utob.c ft_utoh.c ft_utoo.c\
		   ft_wcslen.c ft_xfree.c get_next_line.c gnl.c\
		   getsiz_bin.c getsiz_hex.c getsiz_oct.c\
		   vec_add.c vec_create.c vec_free.c vec_getindex.c vec_init.c\
		   vec_realloc.c vec_set.c\
		   ft_xmalloc.c ft_calloc.c memwatch.c

PRINTF_BASE_PATH := ./libftprintf
PRINTF_SRC_NAME := 	arg_worker.c color.c env.c ft_printf.c ft_putbuff.c\
					wstr_acc.c printf_binary.c printf_char.c printf_d.c\
					printf_h.c printf_octal.c printf_pointer.c printf_percent.c\
					printf_string.c printf_u.c printf_wc.c printf_wstr.c set.c\
					transform.c transform2.c uarg_handler.c worker.c


OBJ_PATH := ./.obj/
OBJ_NAME := $(SRC_NAME:.c=.o)
INC_PATH := ./includes
SRC := $(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ := $(addprefix $(OBJ_PATH),$(OBJ_NAME))
INC := $(addprefix -I,$(INC_PATH))

PRINTF_SRC_PATH := $(PRINTF_BASE_PATH)/src/
PRINTF_OBJ_NAME := $(PRINTF_SRC_NAME:.c=.o)
PRINTF_OBJ_PATH := $(PRINTF_BASE_PATH)/.obj/
PRINTF_INC_PATH := $(PRINTF_BASE_PATH)/inc/
PRINTF_SRC := $(addprefix $(PRINTF_SRC_PATH),$(PRINTF_SRC_NAME))
PRINTF_OBJ := $(addprefix $(PRINTF_OBJ_PATH),$(PRINTF_OBJ_NAME))
PRINTF_INC := $(addprefix -I,$(PRINTF_INC_PATH))

CFLAGS := -Wall -Werror -Wextra -march=native -O2 -fno-builtin -g
UNAME := $(shell uname)

ifeq ($(UNAME), Darwin)
ECHO = echo
NORM = norminette
CC = clang
AR = ar -ruvcs
endif
ifeq ($(UNAME), Linux)
ECHO = /bin/echo -e
CC = gcc
NORM = 42norminette
AR = ar -rucvsU
endif

CC ?= gcc
ECHO ?= echo
AR ?= ar -ruvcs
CFLAGS += -DMEMWATCH -DMW_STDIO

all: $(OBJ_PATH) $(NAME) $(PRINTF_OBJ_PATH) $(FTPRINTF)

#debug:
#	@$(ECHO) "\x1b[33mBuilding with debugging options..."
#	$(MAKE)

$(FTPRINTF): $(PRINTF_OBJ)
	@$(ECHO) "\x1b[33m$(FTPRINTF) object files created, Now generating lib...\x1b[0m"
	@$(AR) $(FTPRINTF) $(PRINTF_OBJ)
	@$(ECHO) "\x1b[32mDone!\n$(FTPRINTF) compilation sucessful!\x1b[0m"


$(NAME): $(OBJ)
	@$(ECHO) "\x1b[33m$(NAME) object files created, Now generating lib...\x1b[0m"
	@$(AR) $(NAME) $?
	@$(ECHO) "\x1b[32mDone!\n$(NAME) compilation sucessful!\x1b[0m"

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	$(CC) $(CFLAGS) $(INC) -c $< -o $@

$(PRINTF_OBJ_PATH)%.o: $(PRINTF_SRC_PATH)%.c
	$(CC) $(CFLAGS) -fvisibility=hidden $(INC) $(PRINTF_INC) -c $< -o $@

$(OBJ_PATH):
	mkdir -p $(OBJ_PATH)

$(PRINTF_OBJ_PATH):
	mkdir -p $(PRINTF_OBJ_PATH)

so:
	gcc -fPIC -shared $(CFLAGS) $(OBJ) -o libft.so

clean:
	@$(ECHO) "\x1b[33mCleaning $(FTPRINTF) object files...\x1b[0m"
	$(RM) $(PRINTF_OBJ)
	@$(ECHO) "\x1b[32mDone!\n\x1b[33mCleaning $(NAME) object files...\x1b[0m"
	$(RM) $(OBJ)
	@$(ECHO) "\x1b[32mDone!\n\x1b[33mRemoving $(FTPRINTF) obj folder...\x1b[0m"
	rmdir $(PRINTF_OBJ_PATH) 2>/dev/null || true
	@$(ECHO) "\x1b[32mDone! $(FTPRINTF) cleaned up!\n\x1b[33mRemoving $(NAME) obj folder...\x1b[0m"
	rmdir $(OBJ_PATH) 2>/dev/null || true
	@$(ECHO) "\x1b[32mDone! $(NAME) cleaned up!\x1b[0m"

fclean: clean
	@$(ECHO) "\x1b[33mDeleting $(FTPRINTF)...\x1b[0m"
	$(RM) $(FTPRINTF)
	@$(ECHO) "\x1b[32m$(FTPRINTF) Removed!\n\x1b[33mDeleting $(NAME)...\x1b[0m"
	$(RM) $(NAME)
	@$(ECHO) "\x1b[32m$(NAME) Removed!\x1b[0m"

re: fclean all

norme:
	$(NORM) $(SRC)
	$(NORM) $(INC_PATH)

bonus: all

.NOTPARALLEL: re
.PHONY: all clean fclean re norme test debug
