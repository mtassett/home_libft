#include "libft.h"
#include <stdio.h>

void	print_lst(void *addr)
{
	printf("|%s|\n", addr);
}

void	*f(void *addr)
{
	if (((char*)addr)[1] == '1')
		return (ft_strdup("aaaaaaaaa"));
	if (((char*)addr)[1] == '2')
		return (ft_strdup("bbbbbbbbb"));
	if (((char*)addr)[1] == '3')
		return (ft_strdup("ccccccccc"));
}

void	del(void *addr)
{
	(void)addr;
}

int		main()
{
	char	*s1, *s2, *s3;
	t_list *l1, *l2, *l3, *n1;

	s1 = malloc(32);
	s2 = malloc(32);
	s3 = malloc(32);
	bzero(s1, 32);
	memset(s1, '1', 30);
	bzero(s2, 32);
	memset(s2, '2', 30);
	bzero(s3, 32);
	memset(s3, '3', 30);
	l1 = ft_lstnew(s1);
	l2 = ft_lstnew(s2);
	l3 = ft_lstnew(s3);
	l1->next = l2;
	ft_print_memory(l1, sizeof(t_list));
	l2->next = l3;
	ft_print_memory(l2, sizeof(t_list));
	ft_print_memory(l3, sizeof(t_list));
	ft_lstiter(l1, print_lst);
	n1 = ft_lstmap(l1, f, del);
	ft_lstiter(n1, print_lst);
	return (0);
}
