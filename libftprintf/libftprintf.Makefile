#* ************************************************************************** *#
#*                                                                            *#
#*                                                        :::      ::::::::   *#
#*   Makefile                                           :+:      :+:    :+:   *#
#*                                                    +:+ +:+         +:+     *#
#*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        *#
#*                                                +#+#+#+#+#+   +#+           *#
#*   Created: 2015/11/10 19:45:01 by mtassett          #+#    #+#             *#
#*   Updated: 2016/07/28 20:08:25 by mtassett         ###   ########.fr       *#
#*                                                                            *#
#* ************************************************************************** *#

NAME = libftprintf.a

SRC_NAME = 	arg_worker.c color.c env.c ft_printf.c ft_putbuff.c wstr_acc.c\
			printf_binary.c printf_char.c printf_d.c printf_h.c printf_octal.c\
			printf_pointer.c printf_percent.c printf_string.c printf_u.c\
			printf_wc.c printf_wstr.c set.c transform.c transform2.c\
			uarg_handler.c worker.c

BASE_PATH= ./libftprintf
SRC_PATH = 	$(BASE_PATH)/src/

OBJ_NAME = 	$(SRC_NAME:.c=.o)
OBJ_PATH = 	$(BASE_PATH)/.obj/
INC_PATH = 	$(BASE_PATH)/inc/

SRC = $(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH),$(OBJ_NAME))
INC = $(addprefix -I,$(INC_PATH))

CFLAGS = -Wall -Wextra -O3 -march=native -Winline -fno-builtin -Werror -fvisibility=hidden -flto
UNAME = $(shell uname)

ifeq ($(UNAME), Darwin)
ECHO = echo
NORM = norminette
CC = clang
AR = ar -ruvcs
#CFLAGS += -flto
endif
ifeq ($(UNAME), Linux)
ECHO = /bin/echo -e
CC = gcc
NORM = 42norminette
AR = ar -rucvsU
CFLAGS += -Wno-unused-result
endif

CC ?= gcc
ECHO ?= echo
AR ?= ar -ruvcsU

all: $(OBJ_PATH) $(NAME)

$(NAME): $(OBJ)
	@$(ECHO) "\x1b[33mObject files created, now generating $(NAME)...\x1b[0m"
	$(AR) $(NAME) $?
	@$(ECHO) "\x1b[32mDone!\n$(NAME) compilation sucessful!\x1b[0m"

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	$(CC) $(CFLAGS) -o $@ -c $<

$(OBJ_PATH):
	mkdir -p $(OBJ_PATH)

clean:
	@$(ECHO) "\x1b[33mCleaning $(NAME) object files...\x1b[0m"
	rm -f $(OBJ)
	@$(ECHO) "\x1b[32mDone!\x1b[0m"
	@$(ECHO) "\x1b[33mRemoving $(NAME) object folder...\x1b[0m"
	@rmdir $(OBJ_PATH) 2>/dev/null || true
	@$(ECHO) "\x1b[32mDone!\n$(NAME) cleaned up!\x1b[0m"

fclean:
	@$(ECHO) "\x1b[33mDeleting $(NAME)...\x1b[0m"
	rm -f $(NAME)
	@$(ECHO) "\x1b[33mDone!\n$(NAME) force cleaned up!\x1b[0m"

re:
	$(MAKE) fclean
	$(MAKE) all

norme:
	$(NORM) $(SRC)
	$(NORM) $(INC_PATH)

.PHONY: all clean fclean re norme test debug
