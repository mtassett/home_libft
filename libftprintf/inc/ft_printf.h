/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/17 16:50:56 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/23 20:30:42 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include "libft.h"
# include <wchar.h>
# define PRINTF_BUFF	2047
# define SHARP			0x1
# define ZERO			0x2
# define MINUS			0x4
# define PLUS			0x8
# define SPACE			0x10
# define WIDTH			0x20
# define ACC			0x40
# define OCT			0x80
# define HEX			0x100

typedef struct	s_col
{
	char const	*c_color;
	uint8_t		c_siz;
	char const	*c_code;
}				t_col;

typedef struct	s_arg
{
	char		a_type;
	char		a_mod;
	uint32_t	a_siz;
	uint32_t	a_prec;
	uint16_t	a_args;
	char		*a_add;
	uint8_t		a_addsiz;
}				t_arg;

typedef struct	s_env
{
	uint32_t	w_total;
	uint32_t	w_cur;
	char		*start;
	char const	*istr;
	char const	*ilst;
	va_list		*va;
}				t_env;

/*
** ft_printf.c
*/

int				ft_printf(char const *str, ...);
void			printf_parser(t_env *e);
void			percent(t_env *e);

/*
** color.c
*/

void			color_set(t_env *e);

/*
** worker.c
*/

void			worker(t_env *e);
void			arg_handler(t_env *e, t_arg *arg);
void			append_zero(t_str *s, size_t new_siz);

/*
** set.c
*/

void			set_flag(char const c, t_arg *arg);
void			set_siz(t_env *e, t_arg *arg, bool width);
void			set_mod(t_env *e, t_arg *arg);
void			set_wildcard(t_env *e, t_arg *arg);

/*
** print_*.c
*/

t_str			*tobin(uint64_t n);
t_str			*tohex(uint64_t n);
t_str			*printf_binary(t_arg *arg, uint64_t u);
t_str			*printf_char(t_arg *arg, char c);
t_str			*printf_d(t_arg *arg, int64_t n);
t_str			*printf_h(t_arg *arg, uint64_t u);
t_str			*printf_octal(t_arg *arg, uint64_t u);
t_str			*printf_pointer(t_arg *arg, void *addr);
t_str			*printf_string(t_arg *arg, char *str);
t_str			*printf_u(t_arg *arg, uint64_t u);
t_str			*printf_wc(wchar_t wc);
t_str			*printf_wstr(t_arg *arg, wchar_t *str);
t_str			*printf_percent(void);
t_str			*wstr_acc(t_str *new, wchar_t *wstr, uint32_t siz);

/*
** transform[2].c
*/

t_str			*transform(t_env *e, t_arg *arg);
t_str			*trans_nomod(t_env *e, t_arg *arg);
t_str			*trans_l(t_env *e, t_arg *arg);
t_str			*trans_ul(t_env *e, t_arg *arg);
t_str			*trans_h(t_env *e, t_arg *arg);
t_str			*trans_uh(t_env *e, t_arg *arg);
t_str			*trans_j(t_env *e, t_arg *arg);
t_str			*trans_z(t_env *e, t_arg *arg);

/*
** ft_putbuff.c
*/

void			ft_buffchar(t_env *e, char c);
void			ft_buffset(t_env *e, char c, size_t siz);
void			ft_putnbuff(t_env *e, char const *add, size_t n);
void			printf_buff(t_env *e);

/*
** uarg_handler.c
*/

void			uarg_handler(t_env *e, t_arg *arg, char *new, size_t new_siz);

/*
** env.c
*/

t_env			*env_init(char const *istr, va_list *va);
void			env_free(t_env *e);
void			die(char const *str, char const *func);

/*
** arg_worker.c
*/

void			arg_worker(t_env *e, t_arg *arg, char *new, size_t new_siz);

#endif
