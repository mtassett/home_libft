/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_char.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/06 15:37:24 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 19:52:12 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

t_str	*printf_char(t_arg *arg, char c)
{
	t_str	*new;

	new = malloc(sizeof(t_str));
	new->s_str = malloc(2);
	*new->s_str = c;
	*(new->s_str + 1) = '\0';
	new->s_siz = 1;
	if (arg->a_args & ACC)
		append_zero(new, 1);
	return (new);
}
