/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/16 19:54:28 by mtassett          #+#    #+#             */
/*   Updated: 2016/10/19 18:27:33 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

/*
** t_env handling, creation, deletion, error function
*/

t_env	*env_init(char const *str, va_list *va)
{
	t_env		*new;
	static char	buff[PRINTF_BUFF + 1] = {0};

	if (!(new = ft_memalloc(sizeof(t_env))))
		die("Environement malloc", __func__);
	new->start = &buff[0];
	new->istr = str;
	new->ilst = str;
	new->w_total = 0;
	new->w_cur = 0;
	new->va = va;
	return (new);
}

void	env_free(t_env *e)
{
	free(e);
}

void	die(char const *str, char const *func)
{
	write(2, str, ft_strlen(str));
	write(2, "failed in ", 10);
	write(2, func, ft_strlen(func));
	exit(-1);
}
