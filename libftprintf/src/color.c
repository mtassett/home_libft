/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/25 20:18:39 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 19:09:35 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

/*
** Handle color code, write to buffer and increments counters
*/

void	color_set(t_env *e)
{
	t_col const		col[] = {
		{"{red}", 5, "\x1b[31m"},
		{"{green}", 7, "\x1b[32m"},
		{"{yellow}", 8, "\x1b[33m"},
		{"{blue}", 6, "\x1b[34m"},
		{"{magenta}", 9, "\x1b[35m"},
		{"{cyan}", 6, "\x1b[36m"},
		{"{reset}", 7, "\x1b[0m"}};
	uint_fast8_t	i;

	i = 0;
	ft_putnbuff(e, e->ilst, e->istr - e->ilst);
	while (++i < 7)
	{
		if (ft_strnequ(e->istr, col[i].c_color, col[i].c_siz))
		{
			ft_putnbuff(e, col[i].c_code, col[i].c_siz);
			e->ilst = e->istr + col[i].c_siz;
			e->w_cur = e->w_cur + col[i].c_siz;
			return ;
		}
	}
	ft_putnbuff(e, e->istr, 1);
	e->istr++;
	e->ilst = e->istr;
}
