/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/01 19:28:48 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 19:59:45 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

void	set_flag(char const c, t_arg *arg)
{
	if (c == '-')
	{
		arg->a_args |= MINUS;
		arg->a_args &= ~(ZERO);
	}
	else if (c == '+')
	{
		arg->a_args |= PLUS;
		arg->a_args &= ~(SPACE);
	}
	else if (c == ' ')
		arg->a_args |= SPACE;
	else if (c == '#')
		arg->a_args |= SHARP;
	else if (c == '0')
		arg->a_args |= ZERO;
}

void	set_siz(t_env *e, t_arg *arg, bool width)
{
	if (width)
	{
		arg->a_args |= WIDTH;
		arg->a_siz = (uint32_t)ft_atoi(e->istr);
	}
	else if (*(e->istr + 1) != '*')
	{
		arg->a_args |= ACC;
		arg->a_args &= ~(SPACE);
		e->istr++;
		arg->a_prec = (uint32_t)ft_atoi(e->istr);
	}
	else
		return ;
	while (ft_isdigit(*e->istr))
		e->istr++;
	e->istr--;
}

void	set_mod(t_env *e, t_arg *arg)
{
	if (*e->istr == 'h')
		if (*(e->istr + 1) == 'h')
		{
			arg->a_mod = 'H';
			++e->istr;
		}
		else
			arg->a_mod = 'h';
	else if (*e->istr == 'l')
		if (*(e->istr + 1) == 'l')
		{
			arg->a_mod = 'L';
			++e->istr;
		}
		else
			arg->a_mod = 'l';
	else if (*e->istr == 'j')
		arg->a_mod = 'j';
	else if (*e->istr == 'z')
		arg->a_mod = 'z';
}

void	set_wildcard(t_env *e, t_arg *arg)
{
	int	n;

	if (*(e->istr - 1) == '.')
	{
		n = va_arg(*e->va, int);
		if (n >= 0)
		{
			arg->a_prec = (uint32_t)n;
			arg->a_args |= ACC;
		}
	}
	else
	{
		n = va_arg(*e->va, int);
		if (n < 0)
		{
			arg->a_args |= MINUS;
			n = -n;
		}
		arg->a_siz = (uint32_t)n;
		arg->a_args |= WIDTH;
	}
}
