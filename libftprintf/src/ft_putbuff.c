/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putbuff.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/16 17:09:33 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 19:06:43 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

void	ft_buffset(t_env *e, char c, size_t siz)
{
	if (e->w_cur + siz >= PRINTF_BUFF)
	{
		printf_buff(e);
		while (siz >= PRINTF_BUFF)
		{
			e->w_cur = PRINTF_BUFF;
			ft_memset(e->start, c, PRINTF_BUFF);
			printf_buff(e);
			siz -= PRINTF_BUFF;
		}
	}
	ft_memset(e->start + e->w_cur, c, siz);
	e->w_cur += siz;
}

void	ft_buffchar(t_env *e, char c)
{
	if (e->w_cur + 1 > PRINTF_BUFF)
		printf_buff(e);
	*(e->start + e->w_cur) = c;
	e->w_cur++;
}

void	ft_putnbuff(t_env *e, char const *add, size_t siz)
{
	if (e->w_cur + siz >= PRINTF_BUFF)
	{
		printf_buff(e);
		while (siz >= PRINTF_BUFF)
		{
			e->w_cur = PRINTF_BUFF;
			ft_memcpy(e->start, add, PRINTF_BUFF);
			printf_buff(e);
			siz -= PRINTF_BUFF;
		}
	}
	if (siz == 1)
		*(e->start + e->w_cur) = *add;
	else
		ft_memcpy(e->start + e->w_cur, add, siz);
	e->w_cur += siz;
}

void	printf_buff(t_env *e)
{
	write(1, e->start, e->w_cur);
	e->w_total += e->w_cur;
	ft_bzero(e->start, e->w_cur + 1);
	e->w_cur = 0;
}
