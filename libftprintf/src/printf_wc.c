/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_wc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/06 17:19:48 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 19:58:39 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

static inline void	dble(t_str *new, wchar_t wc)
{
	*new->s_str = (char)((wc >> 6) + 0xC0);
	*(new->s_str + 1) = (char)((wc & 0x3F) + 0x80);
	new->s_siz = 2;
}

static inline void	trple(t_str *new, wchar_t wc)
{
	*new->s_str = (char)((wc >> 12) + 0xE0);
	*(new->s_str + 1) = (char)(((wc >> 6) & 0x3F) + 0x80);
	*(new->s_str + 2) = (char)((wc & 0x3F) + 0x80);
	new->s_siz = 3;
}

static inline void	quad(t_str *new, wchar_t wc)
{
	*new->s_str = (char)((wc >> 18) + 0xF0);
	*(new->s_str + 1) = (char)(((wc >> 12) & 0x3F) + 0x80);
	*(new->s_str + 2) = (char)(((wc >> 6) & 0x3F) + 0x80);
	*(new->s_str + 3) = (char)((wc & 0x3F) + 0x80);
	new->s_siz = 4;
}

t_str				*printf_wc(wchar_t wc)
{
	t_str	*new;

	if (!(new = malloc(sizeof(t_str))))
		die("t_str malloc", __func__);
	if (!(new->s_str = malloc(sizeof(wchar_t) + 1)))
		die("wchar_t str malloc", __func__);
	*(new->s_str + sizeof(wchar_t)) = L'\0';
	if (wc <= 0x7F)
	{
		*new->s_str = (char)wc;
		new->s_siz = 1;
	}
	else if (wc <= 0x7FF)
		dble(new, wc);
	else if (wc <= 0xFFFF)
		trple(new, wc);
	else if (wc <= 0x10FFFF)
		quad(new, wc);
	return (new);
}
