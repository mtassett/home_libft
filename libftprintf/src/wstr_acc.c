/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wstr_acc.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 20:04:17 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 20:02:36 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

static char	*norme2(char *tmp, wchar_t wc, t_str *new, uint32_t siz)
{
	if (wc <= 0xFFFF)
	{
		if (new->s_siz + 3 > siz)
			return (NULL);
		*tmp = (char)((wc >> 12) + 0xE0);
		*(tmp + 1) = (char)(((wc >> 6) & 0x3F) + 0x80);
		*(tmp + 2) = (char)((wc & 0x3F) + 0x80);
		new->s_siz += 3;
		return (tmp + 3);
	}
	else if (wc <= 0x10FFFF)
	{
		if (new->s_siz + 4 > siz)
			return (NULL);
		*tmp = (char)((wc >> 18) + 0xF0);
		*(tmp + 1) = (char)(((wc >> 12) & 0x3F) + 0x80);
		*(tmp + 2) = (char)(((wc >> 6) & 0x3F) + 0x80);
		*(tmp + 3) = (char)((wc & 0x3F) + 0x80);
		new->s_siz += 4;
		return (tmp + 4);
	}
	else
		return (NULL);
}

static char	*norme(char *tmp, wchar_t wc, t_str *new, uint32_t siz)
{
	if (wc <= 0x7F)
	{
		if (new->s_siz + 1 > siz)
			return (NULL);
		*tmp = (char)wc;
		new->s_siz++;
	}
	else if (wc <= 0x7FF)
	{
		if (new->s_siz + 2 > siz)
			return (NULL);
		*tmp = (char)((wc >> 6) + 0xC0);
		*(tmp + 1) = (char)((wc & 0x3F) + 0x80);
		new->s_siz += 2;
		return (tmp + 2);
	}
	else
		return (norme2(tmp, wc, new, siz));
	return (NULL);
}

t_str				*wstr_acc(t_str *new, wchar_t *wstr, uint32_t siz)
{
	char	*tmp;
	wchar_t	wc;

	new->s_str = ft_memalloc(siz);
	new->s_siz = 0;
	tmp = new->s_str;
	while ((wc = *wstr))
	{
		if (!(tmp = norme(tmp, wc, new, siz)))
			return (new);
		wstr++;
	}
	return (new);
}
