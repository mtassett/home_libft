/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_b.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/17 21:09:14 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 19:55:46 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

static t_str	*tooct(uint64_t n)
{
	static char	const	obase[] = "01234567";
	t_str				*new;
	ssize_t				i;

	new = malloc(sizeof(t_str));
	new->s_siz = getsiz_oct(n);
	i = new->s_siz;
	new->s_str = malloc(new->s_siz + 1);
	new->s_str[i--] = '\0';
	while (i >= 0)
	{
		new->s_str[i--] = obase[n % 8];
		n /= 8;
	}
	return (new);
}

t_str			*printf_octal(t_arg *arg, uint64_t u)
{
	t_str	*new;

	if (arg->a_args & ACC && arg->a_args & ZERO)
		arg->a_args &= ~(ZERO);
	new = tooct(u);
	if (arg->a_args & SHARP)
	{
		if (!(arg->a_args & ACC))
		{
			arg->a_prec = *(new->s_str) == '0' ? new->s_siz : new->s_siz + 1;
			arg->a_args |= ACC;
		}
		else if (arg->a_prec <= new->s_siz)
		{
			if (arg->a_prec == 0 && u != 0)
				arg->a_prec = new->s_siz + 1;
			else
				arg->a_prec++;
		}
	}
	if (arg->a_args & ACC)
		append_zero(new, arg->a_prec);
	return (new);
}
