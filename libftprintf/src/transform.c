/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   transform.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/05 16:14:28 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 20:01:44 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

/*
** Handle variable type transformation (casts), dispatch va_arg to functions
*/

t_str	*transform(t_env *e, t_arg *arg)
{
	if (arg->a_type == 0)
		return (NULL);
	else if (arg->a_type == 's' && arg->a_mod != 'l')
		return (printf_string(arg, va_arg(*e->va, char *)));
	else if (arg->a_type == 'S' || (arg->a_type == 's' && arg->a_mod == 'l'))
		return (printf_wstr(arg, va_arg(*e->va, wchar_t *)));
	else if (arg->a_type == 'U')
		return (printf_u(arg, va_arg(*e->va, unsigned long)));
	else if (arg->a_type == 'D')
		return (printf_d(arg, va_arg(*e->va, long)));
	else if (arg->a_type == 'O')
		return (printf_octal(arg, va_arg(*e->va, unsigned long)));
	else if (arg->a_type == 'C')
		return (printf_wc(va_arg(*e->va, wchar_t)));
	else if (arg->a_type == 'p')
		return (printf_pointer(arg, va_arg(*e->va, void *)));
	else if (arg->a_type == '%')
		return (printf_percent());
	if (!arg->a_mod)
		return (trans_nomod(e, arg));
	else if (arg->a_mod == 'H')
		return (trans_uh(e, arg));
	else if (arg->a_mod == 'h')
		return (trans_h(e, arg));
	else if (arg->a_mod == 'L')
		return (trans_ul(e, arg));
	else if (arg->a_mod == 'l')
		return (trans_l(e, arg));
	else if (arg->a_mod == 'z')
		return (trans_z(e, arg));
	else if (arg->a_mod == 'j')
		return (trans_j(e, arg));
	return (NULL);
}

t_str	*trans_nomod(t_env *e, t_arg *arg)
{
	if (arg->a_type == 's')
		return (printf_string(arg, va_arg(*e->va, char *)));
	else if (arg->a_type == 'S')
		return (printf_wstr(arg, va_arg(*e->va, wchar_t *)));
	else if (arg->a_type == 'd' || arg->a_type == 'i')
		return (printf_d(arg, va_arg(*e->va, int)));
	else if (arg->a_type == 'u')
		return (printf_u(arg, va_arg(*e->va, unsigned)));
	else if (arg->a_type == 'o')
		return (printf_octal(arg, va_arg(*e->va, unsigned)));
	else if (arg->a_type == 'x' || arg->a_type == 'X')
		return (printf_h(arg, va_arg(*e->va, unsigned)));
	else if (arg->a_type == 'c')
		return (printf_char(arg, (unsigned char)va_arg(*e->va, int)));
	else if (arg->a_type == 'C')
		return (printf_wc(va_arg(*e->va, wchar_t)));
	return (NULL);
}

t_str	*trans_uh(t_env *e, t_arg *arg)
{
	if (arg->a_type == 'd' || arg->a_type == 'i')
		return (printf_d(arg, (char)va_arg(*e->va, int)));
	else if (arg->a_type == 'u')
		return (printf_u(arg, (unsigned char)va_arg(*e->va, int)));
	else if (arg->a_type == 'x' || arg->a_type == 'X')
		return (printf_h(arg, (unsigned char)va_arg(*e->va, int)));
	else if (arg->a_type == 'o')
		return (printf_octal(arg, (unsigned char)va_arg(*e->va, int)));
	else if (arg->a_type == 'C')
		return (printf_wc((char)va_arg(*e->va, wchar_t)));
	return (NULL);
}

t_str	*trans_h(t_env *e, t_arg *arg)
{
	if (arg->a_type == 'd' || arg->a_type == 'i')
		return (printf_d(arg, (short)va_arg(*e->va, int)));
	else if (arg->a_type == 'u')
		return (printf_u(arg, (unsigned short)va_arg(*e->va, int)));
	else if (arg->a_type == 'x' || arg->a_type == 'X')
		return (printf_h(arg, (unsigned short)va_arg(*e->va, int)));
	else if (arg->a_type == 'o')
		return (printf_octal(arg, (unsigned short)va_arg(*e->va, int)));
	return (NULL);
}

t_str	*trans_ul(t_env *e, t_arg *arg)
{
	if (arg->a_type == 'd' || arg->a_type == 'i')
		return (printf_d(arg, va_arg(*e->va, long)));
	else if (arg->a_type == 'u')
		return (printf_u(arg, va_arg(*e->va, unsigned long)));
	else if (arg->a_type == 'x' || arg->a_type == 'X')
		return (printf_h(arg, va_arg(*e->va, unsigned long)));
	else if (arg->a_type == 'o')
		return (printf_octal(arg, va_arg(*e->va, unsigned long)));
	return (NULL);
}
