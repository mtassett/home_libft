/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_binary.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/17 21:09:14 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 19:51:45 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

t_str	*tobin(uint64_t n)
{
	static char	const	base[] = "01";
	t_str				*new;

	if (!(new = malloc(sizeof(t_str))))
		die("t_str malloc", __func__);
	new->s_siz = getsiz_bin(n);
	if (!(new->s_str = malloc(new->s_siz + 1)))
		die("str malloc", __func__);
	new->s_str += new->s_siz;
	*(new->s_str--) = '\0';
	while (n)
	{
		*(new->s_str--) = base[n % 2];
		n /= 2;
	}
	return (new);
}

t_str	*printf_binary(t_arg *arg, uint64_t u)
{
	t_str	*new;

	if (arg->a_args & ACC && arg->a_args & ZERO)
		arg->a_args &= ~(ZERO);
	new = tobin(u);
	if (arg->a_args & SHARP)
	{
		arg->a_add = "0b";
		arg->a_addsiz = 2;
	}
	if (arg->a_args & ACC)
		append_zero(new, arg->a_prec);
	return (new);
}
