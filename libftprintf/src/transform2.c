/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   transform2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/11 21:05:13 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 20:00:59 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

/*
** Handle remaining types and dispatch
*/

t_str	*trans_l(t_env *e, t_arg *arg)
{
	if (arg->a_type == 'd' || arg->a_type == 'i' || arg->a_type == 'D')
		return (printf_d(arg, va_arg(*e->va, long long)));
	else if (arg->a_type == 'u')
		return (printf_u(arg, va_arg(*e->va, unsigned long long)));
	else if (arg->a_type == 'x' || arg->a_type == 'X')
		return (printf_h(arg, va_arg(*e->va, unsigned long long)));
	else if (arg->a_type == 'o')
		return (printf_octal(arg, va_arg(*e->va, unsigned long long)));
	else if (arg->a_type == 'c')
		return (printf_wc(va_arg(*e->va, wchar_t)));
	else if (arg->a_type == 's')
		return (printf_wstr(arg, va_arg(*e->va, wchar_t *)));
	return (NULL);
}

t_str	*trans_z(t_env *e, t_arg *arg)
{
	if (arg->a_type == 'd' || arg->a_type == 'i')
		return (printf_d(arg, va_arg(*e->va, long)));
	else if (arg->a_type == 'u')
		return (printf_u(arg, va_arg(*e->va, size_t)));
	else if (arg->a_type == 'x' || arg->a_type == 'X')
		return (printf_h(arg, va_arg(*e->va, size_t)));
	else if (arg->a_type == 'o')
		return (printf_octal(arg, va_arg(*e->va, size_t)));
	return (NULL);
}

t_str	*trans_j(t_env *e, t_arg *arg)
{
	if (arg->a_type == 'd' || arg->a_type == 'i')
		return (printf_d(arg, va_arg(*e->va, intmax_t)));
	else if (arg->a_type == 'u')
		return (printf_u(arg, va_arg(*e->va, uintmax_t)));
	else if (arg->a_type == 'x' || arg->a_type == 'X')
		return (printf_h(arg, va_arg(*e->va, uintmax_t)));
	else if (arg->a_type == 'o')
		return (printf_octal(arg, va_arg(*e->va, uintmax_t)));
	return (NULL);
}
