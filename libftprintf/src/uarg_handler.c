/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   uarg_handler.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/30 16:36:08 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 20:03:05 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

static void	norme1(t_env *e, t_arg arg, char *new, size_t new_siz)
{
	ft_buffset(e, ' ', arg.a_siz - (new_siz + arg.a_addsiz));
	ft_putnbuff(e, arg.a_add, arg.a_addsiz);
	ft_putnbuff(e, new, new_siz);
}

static void	norme2(t_env *e, t_arg arg, char *new, size_t new_siz)
{
	ft_putnbuff(e, arg.a_add, arg.a_addsiz);
	ft_buffset(e, '0', arg.a_siz - (new_siz + arg.a_addsiz));
	ft_putnbuff(e, new, new_siz);
}

void		uarg_handler(t_env *e, t_arg *arg, char *new, size_t new_siz)
{
	if (arg->a_args & (ZERO & WIDTH))
	{
		if (arg->a_siz <= new_siz + arg->a_addsiz)
		{
			ft_putnbuff(e, arg->a_add, arg->a_addsiz);
			ft_putnbuff(e, new, new_siz);
		}
		else
			norme2(e, *arg, new, new_siz);
	}
	else if (arg->a_args & WIDTH)
	{
		if (arg->a_args & MINUS)
			norme1(e, *arg, new, new_siz);
		else
		{
			ft_putnbuff(e, arg->a_add, arg->a_addsiz);
			ft_putnbuff(e, new, new_siz);
			ft_buffset(e, ' ', arg->a_siz - (new_siz + arg->a_addsiz));
		}
	}
}
