/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_h.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/17 16:57:29 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 19:54:56 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

t_str			*tohex(uint64_t n)
{
	static char const	hbase[] = "0123456789abcdef";
	t_str				*new;
	ssize_t				i;

	new = (t_str*)malloc(sizeof(t_str));
	new->s_siz = getsiz_hex(n);
	i = new->s_siz;
	new->s_str = (char*)malloc(new->s_siz + 1);
	new->s_str[i--] = '\0';
	while (i >= 0)
	{
		new->s_str[i--] = hbase[n % 16];
		n /= 16;
	}
	return (new);
}

static t_str	*tohex_caps(uint64_t n)
{
	static char const	hbase_caps[] = "0123456789ABCDEF";
	t_str				*new;
	ssize_t				i;

	new = (t_str*)malloc(sizeof(t_str));
	new->s_siz = getsiz_hex(n);
	i = new->s_siz;
	new->s_str = (char*)malloc(new->s_siz + 1);
	new->s_str[i--] = '\0';
	while (i >= 0)
	{
		new->s_str[i--] = hbase_caps[n % 16];
		n /= 16;
	}
	return (new);
}

t_str			*printf_h(t_arg *arg, uint64_t u)
{
	t_str	*new;

	if (arg->a_args & ACC && arg->a_args & ZERO)
		arg->a_args &= ~(ZERO);
	new = (arg->a_type == 'x') ? tohex(u) : tohex_caps(u);
	if (arg->a_args & SHARP && u)
	{
		arg->a_add = (arg->a_type == 'x') ? "0x" : "0X";
		arg->a_addsiz = 2;
	}
	if (arg->a_args & ACC)
		append_zero(new, arg->a_prec);
	return (new);
}
