/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/25 17:52:06 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 19:03:56 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

int	__attribute__((visibility("default")))	ft_printf(const char *restrict str,
		...)
{
	va_list		pa;
	t_env		*e;
	size_t		siz;

	if (!str)
		return (-1);
	e = env_init(str, &pa);
	va_start(pa, str);
	printf_parser(e);
	va_end(pa);
	printf_buff(e);
	siz = e->w_total;
	env_free(e);
	return (siz);
}

void										printf_parser(t_env *e)
{
	e->ilst = e->istr;
	while (*(e->istr))
	{
		if (*(e->istr) == '{')
			color_set(e);
		else if (*(e->istr) == '%' && *(e->istr + 1) != '%')
			worker(e);
		else if (*(e->istr) == '%' && *(e->istr + 1) == '%')
			percent(e);
		else
			++e->istr;
	}
	if (e->istr != e->ilst)
		ft_putnbuff(e, e->ilst, e->istr - e->ilst);
}

void										percent(t_env *e)
{
	if (e->ilst != e->istr)
		ft_putnbuff(e, e->ilst, e->istr - e->ilst);
	ft_putnbuff(e, "%", 1);
	e->istr += 2;
	e->ilst = e->istr;
}
