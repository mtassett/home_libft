/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_u.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/06 02:05:19 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 19:58:15 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

t_str	*printf_u(t_arg *arg, uint64_t u)
{
	t_str	*new;

	if (arg->a_args & ACC && arg->a_args & ZERO)
		arg->a_args &= ~(ZERO);
	if (!(new = malloc(sizeof(t_str))))
		die("t_str malloc", __func__);
	new->s_siz = ft_getulsiz(u);
	if (!(new->s_str = malloc(new->s_siz + 1)))
		die("str malloc", __func__);
	ft_utoa(new->s_str, u, new->s_siz);
	if (arg->a_args & ACC)
		append_zero(new, arg->a_prec);
	return (new);
}
