/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   worker.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 11:18:27 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 20:02:10 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

void	worker(t_env *e)
{
	t_arg	*arg;
	t_str	*str;

	arg = (t_arg*)ft_memalloc(sizeof(t_arg));
	if (e->istr != e->ilst)
		ft_putnbuff(e, e->ilst, e->istr - e->ilst);
	e->istr++;
	arg_handler(e, arg);
	if (arg->a_args & MINUS && arg->a_args & ZERO)
		arg->a_args &= ~(ZERO);
	str = transform(e, arg);
	if (str)
	{
		arg_worker(e, arg, str->s_str, str->s_siz);
		free(str->s_str);
		free(str);
	}
	free(arg);
	e->ilst = e->istr;
}

void	arg_handler(t_env *e, t_arg *arg)
{
	while (*e->istr)
	{
		if (ft_strchr("sSdDoOuUxXcCpib%", *(e->istr)))
		{
			arg->a_type = *e->istr;
			e->istr++;
			break ;
		}
		else if (ft_strchr("-+ #0", *e->istr))
			set_flag(*e->istr, arg);
		else if (ft_strchr("hljz", *e->istr))
			set_mod(e, arg);
		else if (*e->istr <= '9' && *e->istr > '0' && *(e->istr - 1) != '.')
			set_siz(e, arg, true);
		else if (*e->istr == '.')
			set_siz(e, arg, false);
		else if (*e->istr == '*')
			set_wildcard(e, arg);
		else
			break ;
		e->istr++;
	}
}

void	append_zero(t_str *s, size_t new_siz)
{
	char *tmp;

	if (!ft_memcmp(s->s_str, "0", 2) && !new_siz)
	{
		free(s->s_str);
		s->s_str = ft_strdup("");
		s->s_siz = 0;
	}
	else if (new_siz > s->s_siz)
	{
		if (!(tmp = malloc(new_siz + 1)))
			die("append_zero malloc", __func__);
		ft_memset(tmp, '0', new_siz - s->s_siz);
		ft_memcpy(tmp + (new_siz - s->s_siz), s->s_str, s->s_siz + 1);
		free(s->s_str);
		s->s_str = tmp;
		s->s_siz = new_siz;
	}
}
