/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/15 17:54:21 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/06 18:52:38 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"
#define TEST "{%#-08x}"
#define VALUE 42

/*
typedef struct	s_test
{
	const char	*arg;
	const char	*arg2;
}				t_test;

const t_test test[] = {
	{"%5%", NULL},
	{"This is a simple test", NULL},
	{"Simple Test\nNewline", NULL},
	{"", NULL},
	{"\n", NULL},
	{"{%s}", 0},
	{"(%s}", ""}
};

bool p_test(const char *arg, const char *arg2, size_t n)
{
	int ret, ret2;

	printf("---- Test %05zu ----\n\"%s\"\n\"%s\"\nMine: \"", n, arg, arg2); fflush(NULL);
	ret = ft_printf(arg, arg2);
	printf("\"\n--------------------\nReal: \"");
	ret2 = printf(arg, arg2); 
	printf("\"\n--------------------\n");
	fflush(NULL);
	return (ret == ret2);
}
*/

int main(void)
{
	size_t n = 0;
	int i = 2;
//	size_t siz = 7;

//	do {
//	p_test(test[n].arg, test[n].arg2, n);
//	} while (n++ < siz);
	//ft_printf("%%");
	//write(1, "\n", 1);
	//ft_printf("aa%%bb");
	//write(1, "\n", 1);
	//ft_printf("%%%%%%%%%%");
	//write(1, "\n", 1);
	//ft_printf(".%%.%%.%%.%%.%%.%%.%%.%%.%%.");
	//write(1, "\n", 1);
	printf(TEST, VALUE); printf("\n");fflush(NULL);
	return (ft_printf(TEST"\n", VALUE));
//	printf("\n%%\naa%%bb\n%%%%%%%%%%\n.%%.%%.%%.%%.%%.%%.%%.%%.%%\n%");
}
