/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_wstr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/09 17:02:44 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 19:59:03 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

static inline char	*norme1(char *tmp, wchar_t wc, t_str *new)
{
	*tmp = (char)((wc >> 18) + 0xF0);
	*(tmp + 1) = (char)(((wc >> 12) & 0x3F) + 0x80);
	*(tmp + 2) = (char)(((wc >> 6) & 0x3F) + 0x80);
	*(tmp + 3) = (char)((wc & 0x3F) + 0x80);
	new->s_siz += 4;
	return (tmp + 4);
}

static t_str		*nullcase(t_str *new)
{
	new->s_siz = 6;
	new->s_str = ft_strdup("(null)");
	return (new);
}

static inline char	*norme(char *tmp, wchar_t wc, t_str *new)
{
	if (wc <= 0x7F)
	{
		*tmp = (char)wc;
		new->s_siz++;
	}
	else if (wc <= 0x7FF)
	{
		*tmp = (char)((wc >> 6) + 0xC0);
		*(tmp + 1) = (char)((wc & 0x3F) + 0x80);
		new->s_siz += 2;
		return (tmp + 2);
	}
	else if (wc <= 0xFFFF)
	{
		*tmp = (char)((wc >> 12) + 0xE0);
		*(tmp + 1) = (char)(((wc >> 6) & 0x3F) + 0x80);
		*(tmp + 2) = (char)((wc & 0x3F) + 0x80);
		new->s_siz += 3;
		return (tmp + 3);
	}
	else if (wc <= 0x10FFFF)
		return (norme1(tmp, wc, new));
	return (NULL);
}

t_str				*printf_wstr(t_arg *arg, wchar_t *wstr)
{
	t_str	*new;
	char	*tmp;
	wchar_t	wc;

	if (!(new = malloc(sizeof(t_str))))
		die("t_str malloc", __func__);
	if (!wstr)
		return (nullcase(new));
	if (arg->a_args & ACC)
		return (wstr_acc(new, wstr, arg->a_prec));
	if (!(new->s_str = ft_memalloc(((ft_wcslen(wstr) + 1) * sizeof(wchar_t)))))
		die("str malloc", __func__);

	tmp = new->s_str;
	new->s_siz = 0;
	while ((wc = *wstr))
	{
		tmp = norme(tmp, wc, new);
		wstr++;
	}
	return (new);
}
