/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_string.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/05 17:32:43 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 19:57:48 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

static t_str	*nullcase(t_arg *arg, t_str *new)
{
	if (arg->a_args & ACC)
	{
		new->s_str = ft_strdup("");
		new->s_siz = 0;
	}
	else
	{
		new->s_str = ft_strdup("(null)");
		new->s_siz = 6;
	}
	return (new);
}

t_str			*printf_string(t_arg *arg, char *str)
{
	t_str	*new;

	if (!(new = malloc(sizeof(t_str))))
		return (NULL);
	if (!str)
		return (nullcase(arg, new));
	if (arg->a_args & ACC)
	{
		new->s_siz = ft_strlen(str);
		new->s_siz = arg->a_prec < new->s_siz ? arg->a_prec : new->s_siz;
	}
	else
		new->s_siz = ft_strlen(str);
	new->s_str = ft_strsub(str, 0, new->s_siz);
	return (new);
}
