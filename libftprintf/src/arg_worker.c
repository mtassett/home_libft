/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arg_worker.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/31 18:01:56 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 19:09:01 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

/*
** Handle arguments (size, 0, -, X)
*/

static void	norme(t_env *e, t_arg *arg, char *new, size_t new_siz)
{
	if (arg->a_siz <= new_siz + arg->a_addsiz)
	{
		ft_putnbuff(e, arg->a_add, arg->a_addsiz);
		ft_putnbuff(e, new, new_siz);
	}
	else if (arg->a_args & ZERO)
	{
		ft_putnbuff(e, arg->a_add, arg->a_addsiz);
		ft_buffset(e, '0', arg->a_siz - (new_siz + arg->a_addsiz));
		ft_putnbuff(e, new, new_siz);
	}
	else if (arg->a_args & MINUS)
	{
		ft_putnbuff(e, arg->a_add, arg->a_addsiz);
		ft_putnbuff(e, new, new_siz);
		ft_buffset(e, ' ', arg->a_siz - (arg->a_addsiz + new_siz));
	}
	else
	{
		ft_buffset(e, ' ', arg->a_siz - (arg->a_addsiz + new_siz));
		ft_putnbuff(e, arg->a_add, arg->a_addsiz);
		ft_putnbuff(e, new, new_siz);
	}
}

void		arg_worker(t_env *e, t_arg *arg, char *new, size_t new_siz)
{
	if (arg->a_args & WIDTH)
		norme(e, arg, new, new_siz);
	else
	{
		ft_putnbuff(e, arg->a_add, arg->a_addsiz);
		ft_putnbuff(e, new, new_siz);
	}
}
