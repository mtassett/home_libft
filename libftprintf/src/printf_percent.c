/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_percent.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/13 17:52:33 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 19:57:16 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

t_str	*printf_percent(void)
{
	t_str	*new;

	if (!(new = malloc(sizeof(t_str))))
		die("t_str malloc", __func__);
	new->s_str = ft_strdup("%");
	new->s_siz = 1;
	return (new);
}
