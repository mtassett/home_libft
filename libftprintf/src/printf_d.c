/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_d.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/06 01:56:46 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 19:53:50 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

static void	norme(t_str *new, t_arg *arg, int64_t n)
{
	new->s_siz = ft_getlsiz(n);
	if (arg->a_args & PLUS)
	{
		arg->a_add = "+";
		arg->a_addsiz = 1;
	}
	else if (arg->a_args & SPACE)
	{
		arg->a_add = " ";
		arg->a_addsiz = 1;
	}
}

static void	norme2(t_str *new, t_arg *arg, int64_t *n)
{
	if (*n == INT64_MIN)
	{
		new->s_str = ft_strdup("9223372036854775808");
		*n = -1;
		new->s_siz = 19;
	}
	else
	{
		*n = -(*n);
		new->s_siz = ft_getulsiz(*n);
	}
	arg->a_add = "-";
	arg->a_addsiz = 1;
}

t_str		*printf_d(t_arg *arg, int64_t n)
{
	t_str	*new;

	if (arg->a_args & ACC && arg->a_args & ZERO)
		arg->a_args &= ~(ZERO);
	if (!(new = malloc(sizeof(t_str))))
		return (NULL);
	if (n >= 0)
		norme(new, arg, n);
	else
		norme2(new, arg, &n);
	if (n >= 0)
	{
		new->s_str = malloc(new->s_siz + 1);
		new->s_str = ft_utoa(new->s_str, n, new->s_siz);
	}
	if (arg->a_args & ACC)
		append_zero(new, arg->a_prec);
	return (new);
}
