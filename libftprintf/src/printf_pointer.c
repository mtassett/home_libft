/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_pointer.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mtassett <mtassett@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/06 15:40:26 by mtassett          #+#    #+#             */
/*   Updated: 2016/07/28 19:56:23 by mtassett         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"

t_str	*printf_pointer(t_arg *arg, void *addr)
{
	t_str	*new;

	if (!addr)
	{
		if (!(new = malloc(sizeof(t_str))))
			die("t_str malloc", __func__);
		new->s_str = ft_strdup("0");
		new->s_siz = 1;
	}
	else
		new = tohex((uint64_t)addr);
	arg->a_add = "0x";
	arg->a_addsiz = 2;
	if (arg->a_args & ACC)
		append_zero(new, arg->a_prec);
	return (new);
}
